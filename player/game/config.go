package game

//Config holds game configuration received from GM
type Config struct {
	AgentID        int
	AliesIDs       []int
	EnemiesIDs     []int
	LeaderID       int
	TeamID         string
	BoardXSize     int
	BoardYSize     int
	GoalAreaSize   int
	NumberOfPieces int
	NumberOfGoals  int
	Penalties      struct {
		Move                int
		CheckForSham        int
		Discovery           int
		DestroyPiece        int
		PutPiece            int
		InformationExchange int
	}
	ShamPieceProbability float64
	StartingPos          struct {
		X int
		Y int
	}
}

//MakeConfig return new config filled with data from payload of message
func MakeConfig(data map[string]interface{}) *Config {
	c := &Config{}
	c.AgentID = int(data["agentID"].(float64))
	alliesIDs := data["alliesIDs"].([]interface{})
	c.AliesIDs = make([]int, len(alliesIDs))
	for i := range c.AliesIDs {
		c.AliesIDs[i] = int(alliesIDs[i].(float64))
	}
	enemiesIDs := data["enemiesIDs"].([]interface{})
	c.EnemiesIDs = make([]int, len(enemiesIDs))
	for i := range c.EnemiesIDs {
		c.EnemiesIDs[i] = int(enemiesIDs[i].(float64))
	}
	c.TeamID = data["teamID"].(string)
	c.AgentID = int(data["agentID"].(float64))
	c.LeaderID = int(data["leaderID"].(float64))
	c.BoardXSize = int(data["boardSize"].(map[string]interface{})["x"].(float64))
	c.BoardYSize = int(data["boardSize"].(map[string]interface{})["y"].(float64))
	c.GoalAreaSize = int(data["goalAreaSize"].(float64))
	c.NumberOfPieces = int(data["numberOfPieces"].(float64))
	c.NumberOfGoals = int(data["numberOfGoals"].(float64))
	c.Penalties.Move = int(data["penalties"].(map[string]interface{})["move"].(float64))
	c.Penalties.CheckForSham = int(data["penalties"].(map[string]interface{})["checkForSham"].(float64))
	c.Penalties.Discovery = int(data["penalties"].(map[string]interface{})["discovery"].(float64))
	c.Penalties.DestroyPiece = int(data["penalties"].(map[string]interface{})["destroyPiece"].(float64))
	c.Penalties.PutPiece = int(data["penalties"].(map[string]interface{})["putPiece"].(float64))
	c.Penalties.InformationExchange = int(data["penalties"].(map[string]interface{})["informationExchange"].(float64))
	c.ShamPieceProbability = data["shamPieceProbability"].(float64)
	c.StartingPos.X = int(data["position"].(map[string]interface{})["x"].(float64))
	c.StartingPos.Y = int(data["position"].(map[string]interface{})["y"].(float64))
	return c
}
