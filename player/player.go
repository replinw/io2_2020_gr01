package main

import (
	"fmt"
	"net"
	"player/communicator"
	"player/log"
	"player/strategy"
)

func main() {
	log.InitLogger(false, true)
	config := MakeConfig()
	config.Load()
	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%d", config.CsIP, config.CsPort))
	if err != nil {
		log.Fatal("Failed to connect to CS: %s", err.Error())
	}
	comm := communicator.MakeCommunicator(conn)
	strategy.Run(comm, config.TeamID)
	comm.Terminate()
}
