package log

import (
	"fmt"
	"os"
	"runtime/debug"
	"sync"
	"time"

	"github.com/fatih/color"
)

var exitOnError bool
var ouputDebug bool
var mx sync.Mutex

func fail() {
	debug.PrintStack()
	os.Exit(1)
}

//InitLogger set logger flags exitOnError and debug
func InitLogger(exit bool, dbg bool) {
	exitOnError = exit
	ouputDebug = dbg
}

//Debug log - in magenta, visible only with flag -d
func Debug(msg string, params ...interface{}) {
	if !ouputDebug {
		return
	}
	mx.Lock()
	defer mx.Unlock()
	color.Set(color.FgMagenta)
	fmt.Print("[DEBUG ", time.Now().Format("2006-01-02 15:04:05.00"), "] ")
	color.Unset()
	fmt.Printf(msg, params...)
	fmt.Println()
}

//Info - message in green
func Info(msg string, params ...interface{}) {
	mx.Lock()
	defer mx.Unlock()
	color.Set(color.FgGreen)
	fmt.Print("[INFO  ", time.Now().Format("2006-01-02 15:04:05.00"), "] ")
	color.Unset()
	fmt.Printf(msg, params...)
	fmt.Println()
}

//Warn - message in yellow
func Warn(msg string, params ...interface{}) {
	mx.Lock()
	defer mx.Unlock()
	color.Set(color.FgYellow)
	fmt.Print("[WARN  ", time.Now().Format("2006-01-02 15:04:05.00"), "] ")
	color.Unset()
	fmt.Printf(msg, params...)
	fmt.Println()
}

//Error - message in red, exit if flag -e is set
func Error(msg string, params ...interface{}) {
	mx.Lock()
	defer mx.Unlock()
	color.Set(color.FgRed)
	fmt.Print("[ERROR ", time.Now().Format("2006-01-02 15:04:05.00"), "] ")
	color.Unset()
	fmt.Printf(msg, params...)
	fmt.Println()
	if exitOnError {
		fail()
	}
}

//Fatal - message in red, end program
func Fatal(msg string, params ...interface{}) {
	mx.Lock()
	defer mx.Unlock()
	color.Set(color.FgRed, color.Bold)
	fmt.Print("[FATAL ", time.Now().Format("2006-01-02 15:04:05.00"), "] ")
	color.Unset()
	fmt.Printf(msg, params...)
	fmt.Println()
	fail()
}
