package main

import (
	"encoding/json"
	"io/ioutil"
	"math/rand"
	"os"
	"path/filepath"
	"reflect"
	"strconv"
	"testing"
	"time"

	flag "github.com/spf13/pflag"
)

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func compareConfigs(t *testing.T, expected, actual *Config) {
	if expected.CsIP != actual.CsIP {
		t.Errorf("CsIP field is incorrect. Actual: %s Expected: %s", actual.CsIP, expected.CsIP)
	}
	if expected.CsPort != actual.CsPort {
		t.Errorf("CsPort field is incorrect. Actual: %d Expected: %d", actual.CsPort, expected.CsPort)
	}
	if expected.TeamID != actual.TeamID {
		t.Errorf("TeamID field is incorrect. Actual: %s Expected: %s", actual.TeamID, expected.TeamID)
	}
}

func getRandomFilename() string {
	return "config_test_" + time.Now().Format("2006_01_02_15_04_05") + strconv.Itoa(rand.Int())
}

func createTempFile(t *testing.T, data []byte) string {
	ex, err := os.Executable()
	if err != nil {
		t.Error(err)
	}
	path := filepath.Dir(ex)
	path = filepath.Join(path, getRandomFilename())
	file, err := os.Create(path)
	checkError(err)
	defer func() {
		checkError(file.Close())
	}()
	_, err = file.Write(data)
	checkError(err)
	return path
}

func TestCorrectJSONDeserialization(t *testing.T) {
	for _, expectedConfig := range []struct {
		CsIP   string `json:"csIP"`
		CsPort uint16 `json:"csPort"`
		TeamID string `json:"teamID"`
	}{
		{"127.0.0.1", 5237, "red"},
		{"212.77.98.9", 123, "blue"},
	} {
		ec := Config(expectedConfig)
		json, err := json.Marshal(expectedConfig)
		checkError(err)
		config := MakeConfig()
		config.parseJSON(json)
		compareConfigs(t, &ec, config)
	}
}

func TestIncorrectDataJSONDeserialization(t *testing.T) {
	for _, expectedConfig := range []struct {
		CsIP   string `json:"csIP"`
		CsPort uint16 `json:"csPort"`
		TeamID string `json:"teamID"`
	}{
		{"-123.5312", 5237, "red"},
		{"212.77.98.9", 372, "blue"},
		{"127.0.0.1", 0, "red"},
		{"212.77.98.9", 123, "asd"},
	} {
		ec := Config(expectedConfig)
		json, err := json.Marshal(expectedConfig)
		checkError(err)
		config := MakeConfig()
		config.parseJSON(json)
		compareConfigs(t, &ec, config)
	}

}

func TestMissingCsIPJSONDeserialization(t *testing.T) {
	for _, expectedConfig := range []struct {
		CsPort uint16 `json:"csPort"`
		TeamID string `json:"teamID"`
	}{
		{5237, "red"},
	} {
		ec := MakeConfig()
		ec.CsPort = expectedConfig.CsPort
		ec.TeamID = expectedConfig.TeamID
		json, err := json.Marshal(expectedConfig)
		checkError(err)
		config := MakeConfig()
		config.parseJSON(json)
		compareConfigs(t, ec, config)
	}
}

func TestMissingCsPortJSONDeserialization(t *testing.T) {
	for _, expectedConfig := range []struct {
		CsIP   string `json:"csIP"`
		TeamID string `json:"teamID"`
	}{
		{"212.77.98.9", "blue"},
	} {
		ec := MakeConfig()
		ec.CsIP = expectedConfig.CsIP
		ec.TeamID = expectedConfig.TeamID
		json, err := json.Marshal(expectedConfig)
		checkError(err)
		config := MakeConfig()
		config.parseJSON(json)
		compareConfigs(t, ec, config)
	}
}

func TestMissingTeamIDJSONDeserialization(t *testing.T) {
	for _, expectedConfig := range []struct {
		CsIP   string `json:"csIP"`
		CsPort uint16 `json:"csPort"`
	}{
		{"212.77.98.9", 5237},
	} {
		ec := MakeConfig()
		ec.CsIP = expectedConfig.CsIP
		ec.CsPort = expectedConfig.CsPort
		json, err := json.Marshal(expectedConfig)
		checkError(err)
		config := MakeConfig()
		config.parseJSON(json)
		compareConfigs(t, ec, config)
	}
}

func TestExtraFieldsJSONDeserialization(t *testing.T) {
	for _, expectedConfig := range []struct {
		CsIP   string `json:"csIP"`
		CsPort uint16 `json:"csPort"`
		TeamID string `json:"teamID"`
		Junk   string `json:"junk"`
	}{
		{"212.77.98.9", 123, "blue", "HBSDFAI"},
	} {
		ec := MakeConfig()
		ec.CsIP = expectedConfig.CsIP
		ec.CsPort = expectedConfig.CsPort
		ec.TeamID = expectedConfig.TeamID
		json, err := json.Marshal(expectedConfig)
		checkError(err)
		config := MakeConfig()
		config.parseJSON(json)
		compareConfigs(t, ec, config)
	}
}

func TestIncorrectJSONFile(t *testing.T) {
	json := []byte("bdsaiuyda")
	config := MakeConfig()
	config.parseJSON(json)
	compareConfigs(t, MakeConfig(), config)
}

func TestOpenCorrectJSON(t *testing.T) {
	config := &Config{"128.9.2.7", 1234, "blue"}
	json, err := json.Marshal(config)
	checkError(err)
	path := createTempFile(t, json)
	jsonFiledata := openJSON(path)
	if !reflect.DeepEqual(jsonFiledata, json) {
		t.Errorf("Got: %s, Expected: %s", string(jsonFiledata), string(json))
	}
}

func TestOpenIncorrectJSON(t *testing.T) {
	path := createTempFile(t, []byte("bogus_datafhudasihfd fhdusiahasui n\n hufdsihfduis"))
	jsonFiledata := openJSON(path)
	if jsonFiledata != nil {
		t.Errorf("Got: %s, Expected: nil", string(jsonFiledata))
	}
}

func TestOpenNonExtistingJSON(t *testing.T) {
	path := getRandomFilename()
	jsonFiledata := openJSON(path)
	if jsonFiledata != nil {
		t.Errorf("Got: %s, Expected: nil", string(jsonFiledata))
	}
}

func TestParseCLArgsCorrect(t *testing.T) {
	osArgsBackup := os.Args
	defer func() {
		recover()
		os.Args = osArgsBackup
		flag.CommandLine = flag.NewFlagSet(osArgsBackup[0], flag.PanicOnError)
	}()
	os.Args = append(os.Args, "-i", "0.0.0.0", "-p", "1234", "-t", "red")
	flag.CommandLine = flag.NewFlagSet(osArgsBackup[0], flag.PanicOnError)
	conf := MakeConfig()
	conf.parseCLArgs()
	compareConfigs(t, &Config{
		CsIP:   "0.0.0.0",
		CsPort: 1234,
		TeamID: "red"},
		conf)
}

func TestParseCLArgsNonExistingFlag(t *testing.T) {
	osArgsBackup := os.Args
	defer func() {
		recover()
		os.Args = osArgsBackup
		flag.CommandLine = flag.NewFlagSet(osArgsBackup[0], flag.PanicOnError)
		return
	}()
	os.Args = append(os.Args, "-a")
	conf := MakeConfig()
	conf.parseCLArgs()                   //program should panic here
	t.Error("Incorrect flag was parsed") //but since in defer we return it shouldn't go here
}

func TestParseCLArgsBadTeam(t *testing.T) {
	osArgsBackup := os.Args
	defer func() {
		recover()
		os.Args = osArgsBackup
		flag.CommandLine = flag.NewFlagSet(osArgsBackup[0], flag.PanicOnError)
		return
	}()
	os.Args = append(os.Args, "-t", "yellow")
	conf := MakeConfig()
	conf.parseCLArgs()
	if conf.isValid() {
		t.Error("Yellow team was accepeted!")
	}
}

func isConfigPresent() bool {
	exPath, _ := os.Executable()
	path := filepath.Join(filepath.Dir(exPath), "player.json")
	info, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func TestLoadCorrect(t *testing.T) {
	osArgsBackup := os.Args
	defer func() {
		os.Args = osArgsBackup
		flag.CommandLine = flag.NewFlagSet(osArgsBackup[0], flag.PanicOnError)
		return
	}()
	exPath, _ := os.Executable()
	path := filepath.Join(filepath.Dir(exPath), "player.json")
	if isConfigPresent() {
		t.Fatal("File player.json exist, cannot create test config")
	}
	conf := Config{"0.0.0.0", 1234, "blue"}
	json, err := json.Marshal(conf)
	if err != nil {
		t.Fatalf("Marshalling error: %s", err.Error())
	}
	err = ioutil.WriteFile(path, json, 0644)
	if err != nil {
		t.Fatalf("Error writing config: %s", err.Error())
	}
	config := MakeConfig()
	config.Load()
	os.Remove(path)
	compareConfigs(t, &conf, config)
}

func TestLoadFileNotExist(t *testing.T) {
	//if config not exist we use defaults and flags values
	osArgsBackup := os.Args
	defer func() {
		flag.CommandLine = flag.NewFlagSet(osArgsBackup[0], flag.PanicOnError)
		os.Args = osArgsBackup
		return
	}()
	if isConfigPresent() {
		t.Fatal("File player.json exist, cannot do test")
	}
	fallbackConfig := MakeConfig()
	config := MakeConfig()
	config.Load()
	compareConfigs(t, fallbackConfig, config)
}

func TestLoadFileFromConfigAndFlag(t *testing.T) {
	osArgsBackup := os.Args
	defer func() {
		flag.CommandLine = flag.NewFlagSet(osArgsBackup[0], flag.PanicOnError)
		os.Args = osArgsBackup
		return
	}()
	exPath, _ := os.Executable()
	path := filepath.Join(filepath.Dir(exPath), "player.json")
	if isConfigPresent() {
		t.Fatal("File player.json exist, cannot create test config")
	}
	conf := Config{"0.0.0.0", 1234, "blue"}
	json, err := json.Marshal(conf)
	if err != nil {
		t.Fatalf("Marshalling error: %s", err.Error())
	}
	err = ioutil.WriteFile(path, json, 0644)
	if err != nil {
		t.Fatalf("Error writing config: %s", err.Error())
	}
	config := MakeConfig()
	os.Args = append(os.Args, "-t", "red")
	config.Load()
	conf.TeamID = "red"
	os.Remove(path)
	compareConfigs(t, &conf, config)
}
