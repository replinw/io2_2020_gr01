package strategy

import (
	"fmt"
	"player/communicator"
	"player/game"
	"player/log"
)

type coord struct {
	x int
	y int
}

var segments map[int][2]int
var board [][]int
var config game.Config
var curDestination coord
var curPosition coord
var curFreeField coord
var comm *communicator.Communicator
var team []int

const (
	freeField    = 0
	nonFreeField = 1
)

func shouldFinish(msg *communicator.Message) bool {
	return msg.MessageID == communicator.EndGame || (msg.MessageID == communicator.JoinResponse && !msg.Payload["accepted"].(bool))
}

// Run runs debug strategy
func Run(_comm *communicator.Communicator, teamID string) {
	defer func() {
		r := recover()
		if r != "endGame" {
			panic(r)
		}
	}()
	comm = _comm
	comm.SendJoinRequest(teamID)
	if shouldFinish(comm.WaitForMessage()) {
		return
	}
	gameConfig := game.MakeConfig(comm.WaitForMessage().Payload)
	config = *gameConfig
	board = make([][]int, config.BoardXSize)
	for i := 0; i < config.BoardXSize; i++ {
		board[i] = make([]int, config.BoardYSize)
	}
	curPosition = coord{gameConfig.StartingPos.X, gameConfig.StartingPos.Y}
	log.Debug(fmt.Sprintf("%+v\n", gameConfig))
	if config.AgentID != config.LeaderID || len(config.AliesIDs) < 3 {
		takePosition()  //phase 1
		collectPieces() //phase 2
	} else {
		segments = make(map[int][2]int)
		segments[config.AgentID] = [2]int{0, config.BoardXSize}
		disturbEnemies() //phase 3b
	}
}
