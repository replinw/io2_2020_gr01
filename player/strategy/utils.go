package strategy

import (
	"math/rand"
	"player/communicator"
	"sort"
	"strings"
	"time"
)

type action func()

var collisionCounter int = 0
var sleep time.Duration = 0
var lastPhase bool = false

const (
	maxCollisionScore = 21
	onCollision       = 7
	onGoodMove        = -2
	nMovesToReset     = 6
)

func sendInfo(sender int) {
	var distances []int
	for i := 0; i < config.BoardYSize; i++ {
		distances = append(distances, board[i]...)
	}
}

func getInfo(msg *communicator.Message) {
	fields := msg.Payload["distances"].([]int)
	for i := 0; i < config.BoardYSize; i++ {
		row := fields[i*config.BoardXSize : (i+1)*config.BoardXSize]
		for j, f := range row {
			board[i][j] |= f
		}
	}
}

func forceRequest(request action) *communicator.Message {
	var msg *communicator.Message
	for ok := true; ok; ok = (msg.MessageID == communicator.ErrorStillFreeze) {
		request()
		time.Sleep(sleep)
		sleep = 0
		msg = comm.WaitForMessage()
		if msg.MessageID == communicator.IncomingInfoExchangeRequest {
			msg = comm.WaitForMessage()
		}
		if msg.MessageID == communicator.InfoExchangeResponse {
			msg = comm.WaitForMessage()
		}
		if shouldFinish(msg) {
			panic("endGame")
		}
		time.Sleep(time.Millisecond * 12)
	}
	return msg
}

// setSegments divides board and assigning segments to players
func setSegments() {
	team = make([]int, 0, len(config.AliesIDs)+1)
	if len(config.AliesIDs) >= 3 {
		for _, id := range config.AliesIDs {
			if id != config.LeaderID {
				team = append(team, id)
			}
		}
	} else {
		team = append(team, config.AliesIDs...)
	}
	team = append(team, config.AgentID)
	sort.Sort(sort.IntSlice(team))
	X := config.BoardXSize
	n := len(team)
	segments = make(map[int][2]int)
	for i, id := range team {
		segments[id] = [2]int{(i * X) / n, ((i+1)*X)/n - 1}
	}
}

func goToDestination() {
	direction := ""
	switch { //ruszamy się najpierw w osi Y by uniknąć zderzeń
	case curPosition.y > curDestination.y:
		direction = "S"
	case curPosition.y < curDestination.y:
		direction = "N"
	case curPosition.x < curDestination.x:
		direction = "E"
	case curPosition.x > curDestination.x:
		direction = "W"
	default:
		return
	}
	sleep = time.Duration(3*config.Penalties.Move/4) * time.Millisecond
	msg := forceRequest(func() {
		comm.SendMoveRequest(direction)
	})
	curPosition.x = int(msg.Payload["currentPosition"].(map[string]interface{})["x"].(float64))
	curPosition.y = int(msg.Payload["currentPosition"].(map[string]interface{})["y"].(float64))
	if (msg.MessageID == communicator.MoveResponse && !msg.Payload["madeMove"].(bool)) || msg.MessageID == communicator.ErrorMove {
		collisionCounter += onCollision
		if collisionCounter >= maxCollisionScore {
			collisionCounter = 0
			switch direction {
			case "N":
				direction = "S"
			case "S":
				direction = "N"
			case "E":
				direction = "W"
			case "W":
				direction = "E"
			}
			for i := 0; i < nMovesToReset; i++ {
				sleep = time.Duration(3*config.Penalties.Move/4) * time.Millisecond
				msg := forceRequest(func() {
					comm.SendMoveRequest(direction)
				})
				curPosition.x = int(msg.Payload["currentPosition"].(map[string]interface{})["x"].(float64))
				curPosition.y = int(msg.Payload["currentPosition"].(map[string]interface{})["y"].(float64))
			}
			curDestination = coord{config.BoardXSize / 2, config.BoardYSize / 2}
		} else {
			switch rand.Int() % 4 {
			case 0:
				direction = "S"
			case 1:
				direction = "N"
			case 2:
				direction = "E"
			case 3:
				direction = "W"
			}
			sleep = time.Duration(3*config.Penalties.Move/4) * time.Millisecond
			msg := forceRequest(func() {
				comm.SendMoveRequest(direction)
			})
			curPosition.x = int(msg.Payload["currentPosition"].(map[string]interface{})["x"].(float64))
			curPosition.y = int(msg.Payload["currentPosition"].(map[string]interface{})["y"].(float64))
		}
	} else {
		collisionCounter += onGoodMove
	}
}

func fieldClosestToPiece() (field coord) {
	/*
		Poprawki wprowadzone tutaj (przydałby się review):
		1. premiujemy ruch w osi Y - ma to zapobiec przeszkadzaniu sobie
		2. W pewnych sytuacjach możemy wykonać od razu dwa ruchy
	*/
	msg := forceRequest(func() {
		comm.SendDiscoverRequest()
	})
	discoverPayload := msg.Payload
	direction := ""
	minDist := int(discoverPayload["distanceFromCurrent"].(float64))
	distances := make(map[string]int)
	for _, dirString := range []string{"N", "S", "E", "W", "NE", "SE", "SW", "NW"} {
		d, ok := discoverPayload["distance"+dirString].(float64)
		distances[dirString] = int(d)
		if ok && minDist > int(d) {
			minDist = int(d)
			direction = dirString
		}
	}
	field = curPosition
	if direction == "" {
		return
	}
	/*Jeśli kierunek dwuliterowy jest najkrótszy to jest ostro najkrótszy (bo sa na końcu)
	Tak więc opłaca nam się zrobić dwa ruchy na któryś skos
	*/
	if len(direction) == 2 {
		if strings.Contains(direction, "N") {
			field.y++
		}
		if strings.Contains(direction, "S") {
			field.y--
		}
		if strings.Contains(direction, "E") {
			field.x++
		}
		if strings.Contains(direction, "W") {
			field.x--
		}
	} else {
		//jeśli nie jest to badamy czy sąsiednie kierunki są tak samo dobre.
		//jeśli są gorsze, to możemy zrobić kilka ruchów w tym kierunku (bo to znaczy, że jesteśmy w jednej osi z kawałkiem)
		//niestty chyba trzeba to tak wyifować
		switch direction {
		case "N":
			field.y++
			if distances["NE"] >= minDist && distances["NW"] >= minDist {
				field.y += minDist / 2
			}
		case "S":
			field.y--
			if distances["SE"] >= minDist && distances["SW"] >= minDist {
				field.y -= minDist / 2
			}
		case "E":
			field.x++
			if distances["SE"] >= minDist && distances["NE"] >= minDist {
				field.x += minDist / 2
			}
		case "W":
			field.x--
			if distances["SW"] >= minDist && distances["NW"] >= minDist {
				field.x -= minDist / 2
			}
		}
	}
	return
}

func nextFreeField() coord {
	ret := curFreeField
	ret.x++
	if ret.x > segments[config.AgentID][1] {
		ret.x = segments[config.AgentID][0]
		if config.TeamID == "blue" {
			ret.y--
		} else {
			ret.y++
		}
	}
	if ret.y < 0 || ret.y >= config.BoardYSize {
		lastPhase = true
		if config.TeamID == "blue" {
			return coord{config.BoardXSize / 2, config.GoalAreaSize}
		}
		return coord{config.BoardXSize / 2, config.BoardYSize - config.GoalAreaSize - 1}
	}
	return ret
}

func findValidPiece() {
	for {
		for {
			curDestination = fieldClosestToPiece()
			if curDestination == curPosition {
				break
			}
			for curDestination != curPosition {
				goToDestination()
			}
		}
		msg := forceRequest(func() {
			comm.SendPickPieceRequest()
		})
		if msg.MessageID != communicator.PickResponse {
			continue
		}
		msg = forceRequest(func() {
			comm.SendIsShamRequest()
		})
		if msg.MessageID != communicator.ShamResponse || msg.Payload["sham"].(bool) {
			continue
		}
		return
	}
}

func placePieceOnNextFreeField() {
	curDestination = curFreeField
	for curDestination != curPosition {
		goToDestination()
	}
	forceRequest(func() {
		comm.SendPlacePieceRequest()
	})
	curFreeField = nextFreeField()
	if lastPhase {
		curDestination = coord{config.BoardXSize / 2, config.BoardYSize / 2}
		for curDestination != curPosition {
			goToDestination()
		}
	}
}

func takePosition() {
	setSegments()
	//tutaj y się nie zmienia, chcemy najpierw wyrównać x aby uniknąć zderzeń
	curDestination = coord{(segments[config.AgentID][1] + segments[config.AgentID][0]) / 2, curPosition.y}
	for curDestination != curPosition {
		goToDestination()
	}
	//teraz wyrównujemy y
	curDestination = coord{(segments[config.AgentID][1] + segments[config.AgentID][0]) / 2, config.BoardYSize - config.GoalAreaSize/2 - 1}
	curFreeField = coord{segments[config.AgentID][0], config.BoardYSize - config.GoalAreaSize}
	if config.TeamID == "blue" {
		curDestination.y = config.GoalAreaSize / 2
		curFreeField.y = config.GoalAreaSize - 1
	}
	for curDestination != curPosition {
		goToDestination()
	}
}

func collectPieces() {
	for {
		findValidPiece()
		placePieceOnNextFreeField()
		if curFreeField.x == -1 {
			break
		}
	}
}

func disturbEnemies() {
	x := (config.BoardXSize + segments[config.AgentID][1] + segments[config.AgentID][0]) / 4
	y := config.BoardYSize - config.GoalAreaSize - 1
	maxy := config.BoardYSize - 1
	miny := config.BoardYSize - (config.GoalAreaSize + (config.BoardYSize-2*config.GoalAreaSize)/4)
	minx := 0
	maxx := config.BoardXSize
	if config.AgentID == config.LeaderID {
		minx = config.BoardXSize / 4
		maxx = 3 * config.BoardXSize / 4
	}
	if config.TeamID == "red" {
		y = config.GoalAreaSize
		miny = 0
		maxy = config.GoalAreaSize + (config.BoardYSize-2*config.GoalAreaSize)/4
	}
	for {
	SET_POSITION:
		curDestination = coord{x, y}
		for curDestination != curPosition {
			goToDestination()
		}
		for {
			curDestination = fieldClosestToPiece()
			if curDestination == curPosition {
				for {
					for _, id := range config.EnemiesIDs {
						comm.SendInfoExchangeRequest(id)
						time.Sleep(30 * time.Millisecond)
						msg := comm.ReceiveMessage()
						if msg != nil && shouldFinish(msg) {
							panic("endGame")
						}
					}
				}
			}
			for curDestination != curPosition {
				goToDestination()
				if curDestination.y > maxy || curDestination.y < miny || curDestination.x > maxx || curDestination.x < minx {
					goto SET_POSITION
				}
			}

		}
	}
}
