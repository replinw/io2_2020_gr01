package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"

	flag "github.com/spf13/pflag"

	"player/log"
)

const (
	red  = "red"
	blue = "blue"
)

// Config holds necessary information to initialize a player.
// It can be created with json file or via command line arguments
type Config struct {
	CsIP   string `json:"csIP"`
	CsPort uint16 `json:"csPort"`
	TeamID string `json:"teamID"`
}

func (c *Config) isValid() bool {
	//TODO(franek): replace this regexp with builin functions: https://godoc.org/net#IP
	//and generally we should hold IP as IP type
	ipRegexString := `^(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$`
	ipRegex := regexp.MustCompile(ipRegexString)
	err := false
	if !ipRegex.Match([]byte(c.CsIP)) {
		log.Error("Ip of a value %s is an incorrect ip address", c.CsIP)
		err = true
	}
	if c.CsPort < 1024 {
		log.Error("Port of a value %d is an incorrect port", c.CsPort)
		err = true
	}
	if !(c.TeamID == red || c.TeamID == blue) {
		log.Error("Team of a value %s is an incorrect team", c.TeamID)
		err = true
	}
	return !err
}

func openJSON(path string) []byte {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		log.Warn("Cannot read config: %s", err.Error())
		return nil
	}
	if !json.Valid(data) {
		log.Error("%s is not valid JSON file", path)
		return nil
	}
	return data
}

func (c *Config) parseJSON(jsonbuf []byte) {
	conf := MakeConfig()
	err := json.Unmarshal(jsonbuf, conf)
	if err != nil {
		log.Warn("Cannot parse config: %s", err.Error())
	}
	*c = *conf
}

func (c *Config) parseCLArgs() {
	CsIP := flag.StringP("ip", "i", c.CsIP, "communication server ip")
	CsPort := flag.Uint16P("port", "p", c.CsPort, "communication server port")
	TeamID := flag.StringP("team", "t", c.TeamID, "requested team to join (red|blue)")
	flag.Parse()
	c.CsIP = *CsIP
	c.CsPort = *CsPort
	c.TeamID = *TeamID
}

// MakeConfig returns the default config.
func MakeConfig() *Config {
	return &Config{
		CsIP:   "127.0.0.1",
		CsPort: 5000,
		TeamID: "red",
	}
}

//Load config from file or command line
func (c *Config) Load() {
	ex, err := os.Executable()
	if err != nil {
		log.Warn("Cannot find player executable: %s", err.Error())
	}
	jsonBuf := openJSON(filepath.Join(filepath.Dir(ex), "player.json"))
	if jsonBuf != nil {
		c.parseJSON(jsonBuf)
	}
	c.parseCLArgs()
	if !c.isValid() {
		log.Fatal("Exiting due to incorrect values in config")
	}
	log.Info("Set communication server ip %s", c.CsIP)
	log.Info("Set communication server port %d", c.CsPort)
	log.Info("Set requested team %s", c.TeamID)
}
