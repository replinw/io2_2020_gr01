package communicator

import (
	"bufio"
	"net"
	"os"
	"testing"
	"time"
)

var c *Communicator
var rw bufio.ReadWriter

type voidRequestFunc func() int
type stringRequestFunc func(string) int
type intRequestFunc func(int) int
type testFunc func(t *testing.T)

func TestMain(m *testing.M) {
	local, remote := net.Pipe()
	rw = bufio.ReadWriter{
		Reader: bufio.NewReader(remote),
		Writer: bufio.NewWriter(remote),
	}
	c = MakeCommunicator(local)
	result := m.Run()
	c.Terminate()
	remote.Close()
	os.Exit(result)
}

func checkMessageID(t *testing.T, msg *Message, ID int) {
	if msg.MessageID != ID {
		t.Errorf("Message Id is incorrect. Got %d, expected %d\n", msg.MessageID, ID)
	}
}
func checkCorrelationID(t *testing.T, msg *Message, ID int) {
	if msg.CorrelationID != ID {
		t.Errorf("Correlation Id is incorrect. Got %d, expected %d\n", msg.CorrelationID, ID)
	}
}
func checkForEmptyPayload(t *testing.T, msg *Message) {
	if len(msg.Payload) != 0 {
		t.Errorf("Message Payload is not empty. Got %v\n", msg.Payload)
	}
}
func checkForPayloadData(t *testing.T, msg *Message, name string) interface{} {
	if len(msg.Payload) == 0 {
		t.Errorf("Message Payload is empty\n")
	}
	payload, ok := msg.Payload[name]
	if !ok {
		t.Errorf("Message Payload doesn't contains \"%s\"\n", name)
	}
	return payload
}
func checkForStringPayloadData(t *testing.T, msg *Message, name string, data string) {
	payload := checkForPayloadData(t, msg, name)
	if payload.(string) != data {
		t.Errorf("Message Payload is incorrect. Got \"%v\", expected \"%v\"\n", payload, data)
	}
}
func checkForIntPayloadData(t *testing.T, msg *Message, name string, data int) {
	payload := checkForPayloadData(t, msg, name)
	if int(payload.(float64)) != data {
		t.Errorf("Message Payload is incorrect. Got \"%v\", expected \"%v\"\n", payload, data)
	}
}

func getMessage(t *testing.T) *Message {
	msg, err := UnmarshalMessage(readNBytes(rw, readSize(rw)))
	if err != nil {
		t.Error(err.Error())
	}
	return msg
}

func TestEmptyPayloadMessages(t *testing.T) {
	testEmpty := func(method voidRequestFunc, ID int) testFunc {
		return func(t *testing.T) {
			id := method()
			msg := getMessage(t)
			checkMessageID(t, msg, ID)
			checkCorrelationID(t, msg, id)
			checkForEmptyPayload(t, msg)
		}
	}
	t.Run("SendIsShamRequest", testEmpty(c.SendIsShamRequest, IsSham))
	t.Run("SendDestroyPieceRequest", testEmpty(c.SendDestroyPieceRequest, DestroyPiece))
	t.Run("SendPickPieceRequest", testEmpty(c.SendPickPieceRequest, PickPiece))
	t.Run("SendPlacePieceRequest", testEmpty(c.SendPlacePieceRequest, PlacePiece))
	t.Run("SendDiscoverRequest", testEmpty(c.SendDiscoverRequest, Discover))
}

func TestStringPayloadMessages(t *testing.T) {
	testString := func(method stringRequestFunc, ID int, name string, data string) testFunc {
		return func(t *testing.T) {
			id := method(data)
			msg := getMessage(t)
			checkMessageID(t, msg, ID)
			checkCorrelationID(t, msg, id)
			checkForStringPayloadData(t, msg, name, data)
		}
	}
	t.Run("JoinRequestRed", testString(c.SendJoinRequest, JoinRequest, "teamID", "red"))
	t.Run("JoinRequestBlue", testString(c.SendJoinRequest, JoinRequest, "teamID", "blue"))
	t.Run("MoveRequestN", testString(c.SendMoveRequest, MoveRequest, "direction", "N"))
	t.Run("MoveRequestS", testString(c.SendMoveRequest, MoveRequest, "direction", "S"))
	t.Run("MoveRequestE", testString(c.SendMoveRequest, MoveRequest, "direction", "E"))
	t.Run("MoveRequestW", testString(c.SendMoveRequest, MoveRequest, "direction", "W"))
}

func TestIntPayloadMessages(t *testing.T) {
	testInt := func(method intRequestFunc, ID int, name string, data int) testFunc {
		return func(t *testing.T) {
			id := method(data)
			msg := getMessage(t)
			checkMessageID(t, msg, ID)
			checkCorrelationID(t, msg, id)
			checkForIntPayloadData(t, msg, name, data)
		}
	}
	t.Run("InfoExchangeRequest1", testInt(c.SendInfoExchangeRequest, InfoExchangeRequest, "askedAgentID", 1245))
	t.Run("InfoExchangeRequest2", testInt(c.SendInfoExchangeRequest, InfoExchangeRequest, "askedAgentID", 0))
	t.Run("InfoExchangeRequest3", testInt(c.SendInfoExchangeRequest, InfoExchangeRequest, "askedAgentID", 944534))
}

func TestInfoExchangeResponse(t *testing.T) {
	respondToID := 17
	distances := []int{12, 15, 23}
	redTeamGoalAreaInformations := []string{"aaa", "bbbb", "cccc"}
	blueTeamGoalAreaInformations := []string{"ddd", "eee", "fff"}
	id := c.SendInfoExchangeResponse(respondToID, distances, redTeamGoalAreaInformations, blueTeamGoalAreaInformations)
	msg := getMessage(t)
	checkMessageID(t, msg, InfoExchangeResponse)
	checkCorrelationID(t, msg, id)
	checkForIntPayloadData(t, msg, "respondToID", respondToID)
	sliceData := checkForPayloadData(t, msg, "distances").([]interface{})
	for i := range distances {
		if distances[i] != int(sliceData[i].(float64)) {
			t.Errorf("Message Payload is incorrect. Got %d, expected %d\n", int(sliceData[i].(float64)), distances[i])
		}
	}
	sliceData = checkForPayloadData(t, msg, "redTeamGoalAreaInformations").([]interface{})
	for i := range redTeamGoalAreaInformations {
		if redTeamGoalAreaInformations[i] != sliceData[i].(string) {
			t.Errorf("Message Payload is incorrect. Got \"%s\", expected \"%s\"\n", sliceData[i].(string), redTeamGoalAreaInformations)
		}
	}
	sliceData = checkForPayloadData(t, msg, "blueTeamGoalAreaInformations").([]interface{})
	for i := range blueTeamGoalAreaInformations {
		if blueTeamGoalAreaInformations[i] != sliceData[i].(string) {
			t.Errorf("Message Payload is incorrect. Got \"%s\", expected \"%s\"\n", sliceData[i].(string), blueTeamGoalAreaInformations)
		}
	}
}

func TestWaitForMessage(t *testing.T) {
	wait := true
	ch := make(chan bool, 1)
	go func(w *bool) {
		defer func() {
			recover()
			return
		}()
		time.Sleep(3 * time.Millisecond)
		*w = false
		c.in <- makeEmptyRequest(PlacePiece)
		<-ch
		t.Error("WaitForMessage waits too long")
	}(&wait)
	go func() {
		defer func() {
			recover()
		}()
		time.Sleep(10 * time.Millisecond)
		ch <- true
	}()
	msg := c.WaitForMessage()
	if wait {
		t.Errorf("Received message while still waiting")
	}
	checkMessageID(t, msg, PlacePiece)
	checkForEmptyPayload(t, msg)
}

func TestReceiveMessage(t *testing.T) {
	msg := c.ReceiveMessage()
	if msg != nil {
		t.Errorf("Received message but not send anything")
	}

	c.in <- makeEmptyRequest(PlacePiece)

	msg = c.ReceiveMessage()
	checkMessageID(t, msg, PlacePiece)
	checkForEmptyPayload(t, msg)
	msg = c.ReceiveMessage()
	c.ReceiveMessage()
	if msg != nil {
		t.Errorf("Received 2 messages but send only 1")
	}
}
