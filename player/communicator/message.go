package communicator

import (
	"encoding/json"
)

// Types of messages sent by agent
const (
	// IsSham: Zapytanie czy trzymany fragment jest fikcyjny
	IsSham = iota + 1
	// DestroyPiece: Zapytanie o zniszczenie fragmentu
	DestroyPiece
	// Discover: Zapytanie o akcję odkrycia
	Discover
	// InfoExchangeResponse: Odpowiedź na wymianę informacji
	InfoExchangeResponse
	// InfoExchangeRequest: Zapytanie o wymianę informacji
	InfoExchangeRequest
	// JoinRequest: Zapytanie o dołączenie do rozgrywki
	JoinRequest
	// MoveRequest: Zapytanie o ruch
	MoveRequest
	// PickPiece: Zapytanie o podniesienie fragmentu
	PickPiece
	// PlacePiece: Zapytanie o położenie fragmentu
	PlacePiece
)

// Types of messages recieved by agent
const (
	//ShamResponse: Odpowiedź na sprawdzenie fikcyjnosci
	ShamResponse = iota + 101
	//DestroyResponse: Odpowiedź na prośbę o zniszczenie fragmentu
	DestroyResponse
	//DiscoverResponse: Odpowiedź na akcję discovery
	DiscoverResponse
	//EndGame: Wiadomość o zakończeniu gry
	EndGame
	//StartGame: Wiadomość o rozpoczęciu gry
	StartGame
	//IncomingInfoExchangeRequest: Wiadomość przekazująca zapytanie o wymianę informacji do adresata
	IncomingInfoExchangeRequest
	//JoinResponse: Odpowiedź na zapytanie o dołączenie
	JoinResponse
	//MoveResponse: Odpowiedź na zapytanie o ruch
	MoveResponse
	//PickResponse: Odpowiedź na podniesienie kawałka
	PickResponse
	//PlaceResponse: Odpowiedź na położenie kawałka
	PlaceResponse
)

// Types of error messages received by agent
const (
	//ErrorMove: Błędny ruch
	ErrorMove = iota + 901
	//ErrorPickPiece: Błędne odłożenie kawałka
	ErrorPickPiece
	//ErrorPlacePiece: Błędne położenie kawałka
	ErrorPlacePiece
	//ErrorStillFreeze: Nie odczekanie kary
	ErrorStillFreeze
	//ErrorUndefined: Niezdefiniowany błąd
	ErrorUndefined
)

// Message represents a message that is sent/recieved by agent
type Message struct {
	MessageID     int                    `json:"messageID"`
	CorrelationID int                    `json:"correlationID"`
	Payload       map[string]interface{} `json:"payload"`
}

// UnmarshalMessage deserializes message represented by data
func UnmarshalMessage(data []byte) (*Message, error) {
	message := Message{-1, -1, nil}
	if err := json.Unmarshal(data, &message); err != nil {
		return nil, err
	}
	return &message, nil
}
