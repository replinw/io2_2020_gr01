package communicator

import (
	"encoding/json"
	"reflect"
	"testing"
)

func TestUnmarshalMessage(t *testing.T) {
	for _, expected := range []*Message{
		{
			MessageID:     123,
			CorrelationID: 4623,
			Payload: map[string]interface{}{
				"iksde": 5.0,
				"asd":   "dhsu",
				"dshay": []interface{}{1.0, 2.1, 3.5, 4.1, "string"},
			},
		},
	} {
		data, err := json.Marshal(expected)
		if err != nil {
			t.Error(err)
		}
		actual, err := UnmarshalMessage(data)
		if err != nil {
			t.Error(err)
		}
		if !reflect.DeepEqual(actual, expected) {
			t.Errorf("UnmarshalMessage error: actual: %v, expected: %v", actual, expected)
		}
	}
}
