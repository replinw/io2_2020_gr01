package communicator

import (
	"bufio"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"net"
	"player/log"
	"sync"
	"sync/atomic"
	"time"
)

const (
	mqBufferSize  = 128
	maxMessageLen = 1<<16 - 1
)

// Communicator enables sending/receiveing messages through given connection
type Communicator struct {
	conn net.Conn
	rw   bufio.ReadWriter
	in   chan Message
	out  chan Message
	wg   sync.WaitGroup
}

// Terminate terminates
func (c *Communicator) Terminate() {
	c.conn.SetDeadline(time.Now().Add(time.Second / 5))
	close(c.out)
	close(c.in)
	c.wg.Wait()
	c.conn.Close()
}

// MakeCommunicator constructs communicator
func MakeCommunicator(conn net.Conn) *Communicator {
	ret := &Communicator{
		conn: conn,
		rw: bufio.ReadWriter{
			Reader: bufio.NewReader(conn),
			Writer: bufio.NewWriter(conn),
		},
		in:  make(chan Message, mqBufferSize),
		out: make(chan Message, mqBufferSize),
	}
	ret.wg.Add(2)
	finished := false
	go ret.handleIncomingMessages(&finished)
	go ret.handleOutgoingMessages(&finished)
	return ret
}

func makeEmptyRequest(id int) Message {
	return Message{
		MessageID:     id,
		CorrelationID: getCorrelationID(),
		Payload:       make(map[string]interface{}),
	}
}

// SendIsShamRequest schedules IsSham message to be sent
func (c *Communicator) SendIsShamRequest() int {
	m := makeEmptyRequest(IsSham)
	c.out <- m
	return m.CorrelationID
}

// SendDestroyPieceRequest schedules DestroyPiece message to be sent
func (c *Communicator) SendDestroyPieceRequest() int {
	m := makeEmptyRequest(DestroyPiece)
	c.out <- m
	return m.CorrelationID
}

// SendDiscoverRequest schedules Discover message to be sent
func (c *Communicator) SendDiscoverRequest() int {
	m := makeEmptyRequest(Discover)
	c.out <- m
	return m.CorrelationID
}

// SendInfoExchangeResponse schedules InfoExchangeResponse message to be sent
func (c *Communicator) SendInfoExchangeResponse(
	respondToID int,
	distances []int,
	redTeamGoalAreaInformations []string,
	blueTeamGoalAreaInformations []string) int {
	m := Message{
		MessageID:     InfoExchangeResponse,
		CorrelationID: getCorrelationID(),
		Payload: map[string]interface{}{
			"respondToID":                  respondToID,
			"distances":                    distances,
			"blueTeamGoalAreaInformations": blueTeamGoalAreaInformations,
			"redTeamGoalAreaInformations":  redTeamGoalAreaInformations,
		},
	}
	c.out <- m
	return m.CorrelationID
}

// SendInfoExchangeRequest schedules InfoExchange message to be sent
func (c *Communicator) SendInfoExchangeRequest(askedAgentID int) int {
	m := Message{
		MessageID:     InfoExchangeRequest,
		CorrelationID: getCorrelationID(),
		Payload: map[string]interface{}{
			"askedAgentID": askedAgentID,
		},
	}
	c.out <- m
	return m.CorrelationID
}

// SendJoinRequest schedules Join message to be sent
func (c *Communicator) SendJoinRequest(teamID string) int {
	m := Message{
		MessageID:     JoinRequest,
		CorrelationID: getCorrelationID(),
		Payload: map[string]interface{}{
			"teamID": teamID,
		},
	}
	c.out <- m
	return m.CorrelationID
}

// SendMoveRequest schedules Move message to be sent
func (c *Communicator) SendMoveRequest(direction string) int {
	m := Message{
		MessageID:     MoveRequest,
		CorrelationID: getCorrelationID(),
		Payload: map[string]interface{}{
			"direction": direction,
		},
	}
	c.out <- m
	return m.CorrelationID
}

// SendPickPieceRequest schedules PickPiece message to be sent
func (c *Communicator) SendPickPieceRequest() int {
	m := makeEmptyRequest(PickPiece)
	c.out <- m
	return m.CorrelationID
}

// SendPlacePieceRequest schedules PlacePiece request to be sent
func (c *Communicator) SendPlacePieceRequest() int {
	m := makeEmptyRequest(PlacePiece)
	c.out <- m
	return m.CorrelationID
}

// ReceiveMessage tries to get message from message queue and return it.
// If no message was available, returns nil.
func (c *Communicator) ReceiveMessage() *Message {
	select {
	case m, ok := <-c.in:
		if ok {
			return &m
		}
		log.Fatal("Channel has been closed")
	default:
		return nil
	}
	return nil
}

// WaitForMessage blocks until a message is available and returns it.
func (c *Communicator) WaitForMessage() *Message {
	m, ok := <-c.in
	if ok {
		return &m
	}
	log.Fatal("Channel has been closed")
	return nil
}

func (c *Communicator) handleIncomingMessages(finished *bool) {
	defer func() {
		recover()
		*finished = true
		log.Debug("Goroutine handleOutgoingMessages terminates")
		c.wg.Done()
		return
	}()
	for {
		size := readSize(c.rw)
		data := readNBytes(c.rw, size)
		message, err := UnmarshalMessage(data)
		if err != nil {
			log.Error("Unmarshal error: %s", err.Error())
		} else {
			log.Debug("Recieved: " + fmt.Sprintf("%+v", message))
			c.in <- *message
			if message.MessageID == EndGame {
				log.Info("The game has ended.")
				return
			}
			if message.MessageID == JoinResponse && !message.Payload["accepted"].(bool) {
				log.Info("Agent has been refused.")
				return
			}
		}
	}
}

func (c *Communicator) handleOutgoingMessages(finished *bool) {
	defer func() {
		recover()
		log.Debug("Goroutine handleOutgoingMessages terminates")
		c.wg.Done()
		return
	}()
	for {
		m := <-c.out
		s, err := json.Marshal(m)
		if err != nil {
			log.Fatal("Marshaling error: %s", err.Error())
		}
		n := len(s)
		if n > maxMessageLen {
			log.Fatal("Attempt to send message longer than 2^16-1")
		}
		if *finished {
			return
		}
		log.Debug("Sending: " + fmt.Sprintf("%+v", m))
		buff := make([]byte, 2)
		binary.LittleEndian.PutUint16(buff, uint16(n))
		s = append(buff, s...)
		writeSlice(c.rw, s)
	}
}

var nextCorrelatiodID int32 = 0

func getCorrelationID() int {
	return int(atomic.AddInt32(&nextCorrelatiodID, 1))
}
