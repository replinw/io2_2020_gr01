package communicator

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"io"
	"testing"
)

func TestReadSize(t *testing.T) {
	for _, expected := range []uint16{0, 1, 2, 3, 1<<16 - 1, 1<<16 - 2, 1<<16 - 3} {
		rwBuffer := &bytes.Buffer{}
		rw := *bufio.NewReadWriter(bufio.NewReader(rwBuffer), bufio.NewWriter(rwBuffer))
		buff := make([]byte, 2)
		binary.LittleEndian.PutUint16(buff, expected)
		_, err := rw.Write(buff)
		if err != nil {
			t.Error(err.Error())
		}
		if rw.Flush() != nil {
			t.Error(err.Error())
		}
		actual := readSize(rw)
		if actual != expected {
			t.Errorf("Got %d, expected %d\n", actual, expected)
		}
	}
}

func TestReadNBytes(t *testing.T) {
	for _, expected := range []string{
		"",
		"hdfuiashf",
		"DSAGYUsda^ds5a%SA^D\n\n",
		"789456!@#$0000\a\b\f\n\r\t\v\\'\"",
	} {
		rwBuffer := &bytes.Buffer{}
		rw := *bufio.NewReadWriter(bufio.NewReader(rwBuffer), bufio.NewWriter(rwBuffer))
		_, err := rw.Write([]byte(expected))
		if err != nil {
			t.Error(err.Error())
		}
		err = rw.Flush()
		if err != nil {
			t.Error(err.Error())
		}
		actual := string(readNBytes(rw, uint16(len(expected))))
		if actual != expected {
			t.Errorf("Got %s, expected %s\n", actual, expected)
		}
	}
}

func TestWriteSize(t *testing.T) {
	for _, expected := range []uint16{0, 1, 2, 3, 1<<16 - 1, 1<<16 - 2, 1<<16 - 3} {
		rwBuffer := &bytes.Buffer{}
		rw := *bufio.NewReadWriter(bufio.NewReader(rwBuffer), bufio.NewWriter(rwBuffer))
		writeSize(rw, expected)
		buff := make([]byte, 2)
		_, err := io.ReadFull(rw, buff)
		if err != nil {
			t.Error(err.Error())
		}
		actual := binary.LittleEndian.Uint16(buff)
		if actual != expected {
			t.Errorf("Got %d, expected %d\n", actual, expected)
		}
	}
}

func TestWriteSlice(t *testing.T) {
	for _, expected := range []string{
		"",
		"hdfuiashf",
		"DSAGYUsda^ds5a%SA^D\n\n",
		"789456!@#$0000\a\b\f\n\r\t\v\\'\"",
	} {
		rwBuffer := &bytes.Buffer{}
		rw := *bufio.NewReadWriter(bufio.NewReader(rwBuffer), bufio.NewWriter(rwBuffer))
		writeSlice(rw, []byte(expected))
		buff := make([]byte, len(expected))
		_, err := io.ReadFull(rw, buff)
		if err != nil {
			t.Error(err.Error())
		}
		actual := string(buff)
		if actual != expected {
			t.Errorf("Got %s, expected %s\n", actual, expected)
		}
	}
}
