package communicator

import (
	"bufio"
	"encoding/binary"
	"io"
	"net"

	"player/log"
)

func isTimeout(err error) bool {
	if e, ok := err.(*net.OpError); ok {
		return e.Timeout()
	}
	return false
}

// Read size of message - uint16 in little endian
func readSize(rw bufio.ReadWriter) uint16 {
	buff := readNBytes(rw, 2)
	return binary.LittleEndian.Uint16(buff)
}

// Read exactly n bytes from rw
func readNBytes(rw bufio.ReadWriter, n uint16) []byte {
	buff := make([]byte, n)
	_, err := io.ReadFull(rw, buff)
	if err != nil {
		if isTimeout(err) {
			panic(err)
		}
		log.Fatal(err.Error())
	}
	return buff
}

// Write size to rw in little endian
func writeSize(rw bufio.ReadWriter, size uint16) {
	buff := make([]byte, 2)
	binary.LittleEndian.PutUint16(buff, size)
	writeSlice(rw, buff)
}

// Write buff to rw
func writeSlice(rw bufio.ReadWriter, buff []byte) {
	_, err := rw.Write(buff)
	if err != nil {
		if isTimeout(err) {
			panic(err)
		}
		log.Fatal(err.Error())
	}
	err = rw.Flush()
	if err != nil {
		if isTimeout(err) {
			panic(err)
		}
		log.Fatal(err.Error())
	}
}
