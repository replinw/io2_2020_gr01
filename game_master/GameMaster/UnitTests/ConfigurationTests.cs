using GameMaster.Configurating;
using GameMaster.Gameplay;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class ConfigurationTests
    {
        [TestMethod]
        [DataRow(true, "localhost", 5000, 100, 600, 400, 500, 300, 100, 5, 8, 15, 15, 24, 0.6, 5)]      // valid
        [DataRow(false, "localhost", 5000, 0, 600, 400, 500, 300, 100, 5, 8, 15, 15, 24, 0.6, 5)]       // penalty set to 0
        [DataRow(false, "localhost", 5000, -10, 600, 400, 500, 300, 100, 5, 8, 15, 15, 24, 0.6, 5)]     // negative penalty
        [DataRow(false, "", 5000, 100, 600, 400, 500, 300, 100, 5, 8, 15, 15, 24, 0.6, 5)]              // empty ip
        [DataRow(false, "localhost", 950, 100, 600, 400, 500, 300, 100, 5, 8, 15, 15, 24, 0.6, 5)]      // port too small
        [DataRow(false, "localhost", 67500, 100, 600, 400, 500, 300, 100, 5, 8, 15, 15, 24, 0.6, 5)]    // port too big
        [DataRow(false, "localhost", 5000, 100, 600, 400, 500, 300, 100, 5, 8, 15, 15, 24, 1, 5)]       // sham probability set to 1
        [DataRow(false, "localhost", 5000, 100, 600, 400, 500, 300, 100, 5, 8, 15, 15, 24, 0, 5)]       // sham probability set to 0
        [DataRow(false, "localhost", 5000, 100, 600, 400, 500, 300, 100, 5, 8, 15, 15, 24, -0.3, 5)]    // negative sham probability
        [DataRow(false, "localhost", 5000, 100, 600, 400, 500, 300, 100, 5, 8, 15, 15, 24, 0.6, 80)]    // too many players compared to goal area size
        [DataRow(false, "localhost", 5000, 100, 600, 400, 500, 300, 100, 5, 80, 15, 15, 24, 0.6, 5)]    // too many goals compared to goal area size
        [DataRow(false, "localhost", 5000, 100, 600, 400, 500, 300, 100, 12, 0, 15, 15, 24, 0.6, 5)]    // goal area height set to 0
        [DataRow(false, "localhost", 5000, 100, 600, 400, 500, 300, 100, 12, -3, 15, 15, 24, 0.6, 5)]   // negative goal area height
        [DataRow(false, "localhost", 5000, 100, 600, 400, 500, 300, 100, 12, 8, 15, 15, 24, 0.6, 5)]    // goal area too high compared to boardY
        public void Validate_Test(
            bool isValid,
            string CsIP,
            int CsPort,
            int informationExchangePenalty,
            int checkForShamPenalty,
            int discoveryPenalty,
            int putPenalty,
            int destroyPiecePenalty,
            int movePenalty,
            int goalAreaHight,
            int numberOfGoals,
            int numberOfPieces,
            int boardX,
            int boardY,
            double shamPieceProbability,
            int teamSize)
        {
            var configuration = new Configuration()
            {
                informationExchangePenalty = informationExchangePenalty,
                boardX = boardX,
                boardY = boardY,
                checkForShamPenalty = checkForShamPenalty,
                CsIP = CsIP,
                CsPort = CsPort,
                discoveryPenalty = discoveryPenalty,
                goalAreaHight = goalAreaHight,
                movePenalty = movePenalty,
                numberOfGoals = numberOfGoals,
                numberOfPieces = numberOfPieces,
                putPenalty = putPenalty,
                destroyPiecePenalty = destroyPiecePenalty,
                shamPieceProbability = shamPieceProbability,
                teamSize = teamSize
            };

            Assert.AreEqual(isValid, configuration.Validate());
        }
    }
}
