using GameMaster.Configurating;
using GameMaster.Gameplay;
using GameMaster.Communicating;
using GameMasterPayloads = GameMaster.Communicating.GameMasterPayloads;
using PlayerPayloads = GameMaster.Communicating.PlayerPayloads;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Net.Sockets;
using System.Text.Json;
using System;
using System.Threading.Tasks;
using GameMaster.Communicating.MessageTypes;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using GameMaster.Gameplay.Piece;
using GameMaster.Gameplay.Field;

namespace UnitTests
{
    [TestClass]
    public class FieldTests
    {
        [TestMethod]
        [DataRow(10, 20, Team.blue)]
        [DataRow(34, 72, Team.red)]
        public void GetPosition_Test(int y, int x, Team team)
        {
            var field1 = new GoalField(y, x, team);
            var field2 = new NonGoalField(y, x, team);
            var field3 = new TaskField(y, x);

            Assert.AreEqual(field1.GetPosition().Item1, y);
            Assert.AreEqual(field1.GetPosition().Item2, x);
            Assert.AreEqual(field2.GetPosition().Item1, y);
            Assert.AreEqual(field2.GetPosition().Item2, x);
            Assert.AreEqual(field3.GetPosition().Item1, y);
            Assert.AreEqual(field3.GetPosition().Item2, x);
        }

        [TestMethod]
        [DataRow(10, 20, Team.red)]
        [DataRow(34, 72, Team.blue)]
        public void Leave_Test(int y, int x, Team team)
        {
            var field = new GoalField(y, x, team);
            var player = new Player() { position = field };

            typeof(AbstractField).GetProperty("WhosHere").SetValue(field, player);

            field.Leave();

            Assert.AreEqual(null, field.WhosHere);
        }

        [TestMethod]
        [DataRow(10, 20, Team.blue)]
        [DataRow(34, 72, Team.red)]
        public void MoveHere_Test(int y, int x, Team team)
        {
            var field = new GoalField(y, x, team);
            var player = new Player() { position = field };

            field.MoveHere(player);

            Assert.AreEqual(player, field.WhosHere);
            Assert.AreEqual(field, player.position);
        }

        [TestMethod]
        [DataRow(10, 20, Team.red)]
        [DataRow(34, 72, Team.blue)]
        public void ContainsPieces_Test(int y, int x, Team team)
        {
            var field = new GoalField(y, x, team);

            Assert.AreEqual(false, field.ContainsPieces());
        }

        [TestMethod]
        [DataRow(10, 20, Team.red)]
        [DataRow(34, 72, Team.blue)]
        public void PickUpInGoalArea_Test(int y, int x, Team team)
        {
            var field1 = new GoalField(y, x, team);
            var field2 = new NonGoalField(y, x, team);
            var player = new Player();

            field1.PickUp(player);
            field2.PickUp(player);

            Assert.AreEqual(null, player.holding);
        }

        [TestMethod]
        [DataRow(10, 20)]
        [DataRow(34, 72)]
        public void PickUpOnTaskField_Test(int y, int x)
        {
            var field = new TaskField(y, x);
            var player = new Player();
            var piece = new ShamPiece();

            field.pieces.Add(piece);

            field.PickUp(player);

            Assert.AreEqual(piece, player.holding);
        }
    }
}
