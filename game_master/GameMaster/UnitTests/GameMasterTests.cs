using GameMaster.Configurating;
using GameMaster.Gameplay;
using GameMaster.Communicating;
using GameMasterPayloads = GameMaster.Communicating.GameMasterPayloads;
using PlayerPayloads = GameMaster.Communicating.PlayerPayloads;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Net.Sockets;
using System.Text.Json;
using System;
using System.Threading.Tasks;
using GameMaster.Communicating.MessageTypes;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using GameMaster.Gameplay.Piece;
using GameMaster.Gameplay.Field;

namespace UnitTests
{
    [TestClass]
    public class GameMasterTests
    {
        [TestMethod]
        public void InitializeGame_Test()
        {
            var GameMaster = new GM(123);
            var configuration = new Configuration
            {
                teamSize = 5,
                boardX = 10,
                boardY = 20,
                goalAreaHight = 6,
                numberOfGoals = 10,
                numberOfPieces = 16,
                shamPieceProbability = 0.6
            };
            GameMaster.SetConfiguration(configuration);

            var players = new (int, bool, Team)[10] { (1, true, Team.red), (2, false, Team.red), (3, false, Team.red), (4, false, Team.red), (5, false, Team.red),
                (6, true, Team.blue), (7, false, Team.blue), (8, false, Team.blue), (9, false, Team.blue), (10, false, Team.blue) };
            foreach (var player in players)
            {
                GameMaster.Players.Add(player.Item1, new Player()
                {
                    id = player.Item1,
                    isLeader = player.Item2,
                    team = player.Item3
                });
            }

            GameMaster.InitializeGame();

            GameMaster.log.Close();

            Assert.IsTrue(GameMaster.Board
                .Cast<AbstractField>()
                .Count() == configuration.boardX * configuration.boardY);
            Assert.IsTrue(GameMaster.Board
                .Cast<AbstractField>()
                .Where(x => x.y >= configuration.goalAreaHight && x.y < configuration.boardY - configuration.goalAreaHight)
                .All(x => x.GetType() == typeof(TaskField)));
            Assert.IsTrue(GameMaster.Board
                .Cast<AbstractField>()
                .Where(x => x.y < configuration.goalAreaHight && x.y >= configuration.boardY - configuration.goalAreaHight)
                .All(x => x.GetType() == typeof(GoalField) || x.GetType() == typeof(NonGoalField)));
            Assert.IsTrue(GameMaster.Board
                .Cast<AbstractField>()
                .Where(x => x.y < configuration.goalAreaHight)
                .Where(x => x.GetType() == typeof(GoalField))
                .Count() == configuration.numberOfGoals);
            Assert.IsTrue(GameMaster.Board
                .Cast<AbstractField>()
                .Where(x => x.y >= configuration.boardY - configuration.goalAreaHight)
                .Where(x => x.GetType() == typeof(GoalField))
                .Count() == configuration.numberOfGoals);

            Assert.IsTrue(GameMaster.Players
                .All(x => x.Value.position.x >= 0 && x.Value.position.x < configuration.boardX && x.Value.position.y >= 0 && x.Value.position.y < configuration.boardY));
            Assert.IsTrue(GameMaster.Players
                .ToList()
                .All(x => GameMaster.Board
                    .Cast<AbstractField>()
                    .Single(y => y.x == x.Value.position.x && y.y == x.Value.position.y)
                    .WhosHere.id == x.Value.id));
        }

        [TestMethod]
        [DataRow(123)]
        public void GeneratePiece_Test(int seed)
        {
            var GameMaster = new GM(seed);
            var configuration = new Configuration
            {
                teamSize = 5,
                boardX = 10,
                boardY = 20,
                goalAreaHight = 6,
                numberOfGoals = 10,
                numberOfPieces = 16,
                shamPieceProbability = 0.6
            };
            GameMaster.SetConfiguration(configuration);

            var players = new (int, bool, Team)[10] { (1, true, Team.red), (2, false, Team.red), (3, false, Team.red), (4, false, Team.red), (5, false, Team.red),
                (6, true, Team.blue), (7, false, Team.blue), (8, false, Team.blue), (9, false, Team.blue), (10, false, Team.blue) };
            foreach (var player in players)
            {
                GameMaster.Players.Add(player.Item1, new Player()
                {
                    id = player.Item1,
                    isLeader = player.Item2,
                    team = player.Item3
                });
            }

            GameMaster.InitializeGame();

            GameMaster.GetType().GetField("random", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).SetValue(GameMaster, new Random(seed));

            GameMaster.GeneratePiece();

            var random = new Random(seed);

            int x = random.Next(configuration.boardX);
            int y = random.Next(configuration.goalAreaHight, configuration.boardY - configuration.goalAreaHight);
            bool isSham = random.NextDouble() < configuration.shamPieceProbability;

            GameMaster.log.Close();

            if (isSham)
                Assert.IsTrue(GameMaster.Board[y, x].pieces.Last().GetType() == typeof(ShamPiece));
            else
                Assert.IsTrue(GameMaster.Board[y, x].pieces.Last().GetType() == typeof(NormalPiece));
        }

        [TestMethod]
        [DataRow(10, 8, 10, Team.red)]
        [DataRow(7, 10, 10, Team.blue)]
        public void CheckWinner_Test(int redPoints, int bluePoints, int goalsCount, Team team)
        {
            var GameMaster = new GM();
            var configuration = new Configuration
            {
                numberOfGoals = goalsCount
            };
            GameMaster.SetConfiguration(configuration);

            GameMaster.BlueTeamPoints = bluePoints;
            GameMaster.RedTeamPoints = redPoints;

            GameMaster.log.Close();

            var winner = GameMaster.CheckWinner();
            Assert.AreEqual(team, winner);
        }

        [TestMethod]
        [DataRow(null, false)]
        public void ValidateConfigurationIfNull_Test(Configuration configuration, bool isValid)
        {
            var GameMaster = new GM();
            GameMaster.SetConfiguration(configuration);
            GameMaster.log.Close();

            Assert.AreEqual(isValid, GameMaster.ValidateConfiguration());
        }

        [TestMethod]
        [DataRow(100, 10, 20, 600, "localhost", 5000, 400, 5, 300, 10, 12, 200, 180, 0.6, 6)]
        [DataRow(20, 30, 10, 300, "localhost", 5000, 400, 4, 300, 12, 14, 500, 280, 0.8, 3)]
        public void SetConfiguration_Test(
            int informationExchangePenalty,
            int boardX,
            int boardY,
            int checkForShamPenalty,
            string CsIP,
            int CsPort,
            int discoveryPenalty,
            int goalAreaHight,
            int movePenalty,
            int numberOfGoals,
            int numberOfPieces,
            int putPenalty,
            int destroyPiecePenalty,
            double shamPieceProbability,
            int teamSize)
        {
            var configuration = new Configuration()
            {
                informationExchangePenalty = informationExchangePenalty,
                boardX = boardX,
                boardY = boardY,
                checkForShamPenalty = checkForShamPenalty,
                CsIP = CsIP,
                CsPort = CsPort,
                discoveryPenalty = discoveryPenalty,
                goalAreaHight = goalAreaHight,
                movePenalty = movePenalty,
                numberOfGoals = numberOfGoals,
                numberOfPieces = numberOfPieces,
                putPenalty = putPenalty,
                destroyPiecePenalty = destroyPiecePenalty,
                shamPieceProbability = shamPieceProbability,
                teamSize = teamSize
            };

            var GameMaster = new GM();
            GameMaster.SetConfiguration(configuration);
            GameMaster.log.Close();

            Assert.AreEqual(informationExchangePenalty, GameMaster.configuration.informationExchangePenalty);
            Assert.AreEqual(boardX, GameMaster.configuration.boardX);
            Assert.AreEqual(boardY, GameMaster.configuration.boardY);
            Assert.AreEqual(checkForShamPenalty, GameMaster.configuration.checkForShamPenalty);
            Assert.AreEqual(CsIP, GameMaster.configuration.CsIP);
            Assert.AreEqual(CsPort, GameMaster.configuration.CsPort);
            Assert.AreEqual(discoveryPenalty, GameMaster.configuration.discoveryPenalty);
            Assert.AreEqual(goalAreaHight, GameMaster.configuration.goalAreaHight);
            Assert.AreEqual(movePenalty, GameMaster.configuration.movePenalty);
            Assert.AreEqual(numberOfGoals, GameMaster.configuration.numberOfGoals);
            Assert.AreEqual(numberOfPieces, GameMaster.configuration.numberOfPieces);
            Assert.AreEqual(putPenalty, GameMaster.configuration.putPenalty);
            Assert.AreEqual(destroyPiecePenalty, GameMaster.configuration.destroyPiecePenalty);
            Assert.AreEqual(shamPieceProbability, GameMaster.configuration.shamPieceProbability);
            Assert.AreEqual(teamSize, GameMaster.configuration.teamSize);
        }

        [TestMethod]
        [DataRow(123)]
        public void CalculateClosesPiece_Test(int seed)
        {
            var GameMaster = new GM(seed);
            var configuration = new Configuration
            {
                teamSize = 5,
                boardX = 10,
                boardY = 20,
                goalAreaHight = 6,
                numberOfGoals = 10,
                numberOfPieces = 16,
                shamPieceProbability = 0.6
            };
            GameMaster.SetConfiguration(configuration);

            var players = new (int, bool, Team)[10] { (1, true, Team.red), (2, false, Team.red), (3, false, Team.red), (4, false, Team.red), (5, false, Team.red),
                (6, true, Team.blue), (7, false, Team.blue), (8, false, Team.blue), (9, false, Team.blue), (10, false, Team.blue) };
            foreach (var player in players)
            {
                GameMaster.Players.Add(player.Item1, new Player()
                {
                    id = player.Item1,
                    isLeader = player.Item2,
                    team = player.Item3
                });
            }

            GameMaster.InitializeGame();

            int? closest = int.MaxValue;

            for (int y = configuration.goalAreaHight; y < configuration.boardY - configuration.goalAreaHight; y++)
            {
                for (int x = 0; x < configuration.boardX; x++)
                {
                    if (GameMaster.Board[y, x].pieces.Count > 0)
                    {
                        var distance = Math.Abs(x - configuration.boardX / 2) + Math.Abs(y - configuration.boardY / 2);
                        if (distance < closest.Value)
                            closest = distance;
                    }
                }
            }
            if (closest == int.MaxValue)
                closest = null;

            GameMaster.log.Close();

            Assert.AreEqual(closest, GameMaster.CalculateClosestPiece(configuration.boardX / 2, configuration.boardY / 2));
        }
    }
}
