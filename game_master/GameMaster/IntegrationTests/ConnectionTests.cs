using GameMaster.Communicating;
using GameMaster.Configurating;
using GameMaster.Gameplay;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace IntegrationTests
{
    [TestClass]
    public class ConnectionTests
    {
        [TestMethod]
        public async Task ConnectGMToCS_Test()
        {
            var GameMaster = new GM();
            GameMaster.SetConfiguration(new Configuration
            {
                CsIP = "localhost",
                CsPort = 5000
            });

            var server = new TcpListener(IPAddress.Any, 5000);
            server.Start();
            var connected = await GameMaster.ConnectToCommunicationServer();
            Assert.IsTrue(connected);
            server.AcceptTcpClient();

            await GameMaster.Close();
            server.Stop();
        }

        [TestMethod]
        public async Task AwaitForPlayersToJoin_Test()
        {
            var playersCount = 10;

            var GameMaster = new GM();
            GameMaster.SetConfiguration(new Configuration
            {
                CsIP = "localhost",
                CsPort = 5000,
                teamSize = playersCount / 2,
                boardX = 10,
                boardY = 20,
                goalAreaHight = 6,
                numberOfGoals = 10,
                numberOfPieces = 16,
                shamPieceProbability = 0.6,
                informationExchangePenalty = 500,
                checkForShamPenalty = 200,
                discoveryPenalty = 750,
                movePenalty = 400,
                putPenalty = 800
            });

            var server = new TcpListener(IPAddress.Any, 5000);
            server.Start();
            await GameMaster.ConnectToCommunicationServer();
            var client = server.AcceptTcpClient();

            var gm = Task.Run(() => GameMaster.AwaitForPlayersToJoin());

            for (int i = 1; i <= playersCount; i++)
            {
                var message = new Message()
                {
                    agentID = i,
                    messageID = 006,
                    payload = new { teamID = i > playersCount / 2 ? "red" : "blue" }
                };

                var messageJson = JsonConvert.SerializeObject(message);
                var messageBytes = Encoding.UTF8.GetBytes(messageJson);
                var messageSize = Convert.ToInt16(messageBytes.Length);
                var messageSizeBytes = BitConverter.GetBytes(messageSize);
                var stream = client.GetStream();
                stream.Write(messageSizeBytes);
                stream.Write(messageBytes);
            }

            gm.Wait();

            for (int i = 1; i <= playersCount; i++)
            {
                var stream = client.GetStream();
                var messageSizeBuffer = new byte[2];
                stream.Read(messageSizeBuffer, 0, 2);
                var messageSize = BitConverter.ToInt16(messageSizeBuffer);

                if (messageSize > 8192)
                    Assert.Fail();

                var buffer = new byte[messageSize];
                stream.Read(buffer, 0, messageSize);

                string jsonStr = Encoding.UTF8.GetString(buffer);
                var message = JsonConvert.DeserializeObject<Message>(jsonStr);
                Assert.AreEqual(i, message.agentID);

                var payload = (message.payload as JObject).ToObject<GameMaster.Communicating.GameMasterPayloads.Join107>();
                Assert.AreEqual(i, payload.agentID);
                Assert.IsTrue(payload.accepted);
            }

            await GameMaster.Close();
            server.Stop();
        }

        [TestMethod]
        public async Task SendStartGameMessage()
        {
            var GameMaster = new GM();
            GameMaster.SetConfiguration(new Configuration
            {
                CsIP = "localhost",
                CsPort = 5000,
                teamSize = 5,
                boardX = 10,
                boardY = 20,
                goalAreaHight = 6,
                numberOfGoals = 10,
                numberOfPieces = 16,
                shamPieceProbability = 0.6,
                informationExchangePenalty = 500,
                checkForShamPenalty = 200,
                discoveryPenalty = 750,
                movePenalty = 400,
                putPenalty = 800
            });

            var server = new TcpListener(IPAddress.Any, 5000);
            server.Start();
            await GameMaster.ConnectToCommunicationServer();
            var client = server.AcceptTcpClient();

            var gm = Task.Run(() => GameMaster.AwaitForPlayersToJoin());

            for (int i = 1; i <= 10; i++)
            {
                var message = new Message()
                {
                    agentID = i,
                    messageID = 006,
                    payload = new { teamID = i > 5 ? "red" : "blue" }
                };

                var messageJson = JsonConvert.SerializeObject(message);
                var messageBytes = Encoding.UTF8.GetBytes(messageJson);
                var messageSize = Convert.ToInt16(messageBytes.Length);
                var messageSizeBytes = BitConverter.GetBytes(messageSize);
                var stream = client.GetStream();
                stream.Write(messageSizeBytes);
                stream.Write(messageBytes);
            }

            gm.Wait();

            gm = Task.Run(() => GameMaster.InitializeGame());

            gm.Wait();

            gm = Task.Run(() => GameMaster.StartGame());

            gm.Wait();

            for (int i = 1; i <= 10; i++) // join accept messages
            {
                var stream = client.GetStream();
                var messageSizeBuffer = new byte[2];
                stream.Read(messageSizeBuffer, 0, 2);
                var messageSize = BitConverter.ToInt16(messageSizeBuffer);
                var buffer = new byte[messageSize];
                stream.Read(buffer, 0, messageSize);
            }

            for (int i = 1; i <= 10; i++)
            {
                var stream = client.GetStream();
                var messageSizeBuffer = new byte[2];
                stream.Read(messageSizeBuffer, 0, 2);
                var messageSize = BitConverter.ToInt16(messageSizeBuffer);

                if (messageSize > 8192)
                    Assert.Fail();

                var buffer = new byte[messageSize];
                stream.Read(buffer, 0, messageSize);

                string jsonStr = Encoding.UTF8.GetString(buffer);
                var message = JsonConvert.DeserializeObject<Message>(jsonStr);
                Assert.IsTrue(message.agentID >= 1 && message.agentID <= 10);

                var payload = (message.payload as JObject).ToObject<GameMaster.Communicating.GameMasterPayloads.StartGame105>();
                Assert.AreEqual(message.agentID, payload.agentID);
            }

            for (int i = 1; i <= 5; i++)
                Assert.IsTrue(GameMaster.Players.ContainsKey(i) && GameMaster.Players[i].team == Team.blue);

            for (int i = 6; i <= 10; i++)
                Assert.IsTrue(GameMaster.Players.ContainsKey(i) && GameMaster.Players[i].team == Team.red);

            Assert.IsFalse(GameMaster.Players.Keys.Except(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }).Any());

            await GameMaster.Close();
            server.Stop();
        }
    }
}
