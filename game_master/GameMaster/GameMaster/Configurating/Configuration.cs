﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace GameMaster.Configurating
{
    public class Configuration
    {
        [Required]
        public string CsIP { get; set; }

        [Range(1024, 65535)]
        public int CsPort { get; set; }

        [Range(1, int.MaxValue)]
        public int movePenalty { get; set; }

        [Range(1, int.MaxValue)]
        public int informationExchangePenalty { get; set; }

        [Range(1, int.MaxValue)]
        public int discoveryPenalty { get; set; }

        [Range(1, int.MaxValue)]
        public int putPenalty { get; set; }

        [Range(1, int.MaxValue)]
        public int checkForShamPenalty { get; set; }

        [Range(1, int.MaxValue)]
        public int destroyPiecePenalty { get; set; }

        [Range(1, int.MaxValue)]
        public int boardX { get; set; }

        [Range(1, int.MaxValue)]
        public int boardY { get; set; }

        [GoalAreaHeight]
        [Range(1, int.MaxValue)]
        public int goalAreaHight { get; set; }

        [GoalsNumber]
        [Range(1, int.MaxValue)]
        public int numberOfGoals { get; set; }

        [Range(1, int.MaxValue)]
        public int numberOfPieces { get; set; }

        [PlayersNumber]
        [Range(1, int.MaxValue)]
        public int teamSize { get; set; }

        [ShamPieceProbability]
        public double shamPieceProbability { get; set; }

        public static async Task<Configuration> ReadConfigurationFile()
        {
            using (StreamReader reader = new StreamReader("Configurating/GMConfig.json"))
            {
                string json = await reader.ReadToEndAsync();
                try
                {
                    return JsonConvert.DeserializeObject<Configuration>(json);
                }
                catch (JsonException exception)
                {
                    Console.WriteLine("Could not read configuration file: " + exception.Message);
                    return null;
                }
            }
        }

        public bool Validate()
        {
            var context = new ValidationContext(this);
            var list = new List<ValidationResult>();
            return Validator.TryValidateObject(this, context, list, true);
        }

        public class GoalsNumberAttribute : ValidationAttribute
        {
            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                var boardXProperty = validationContext.ObjectType.GetProperty("boardX");
                var goalAreaHightProperty = validationContext.ObjectType.GetProperty("goalAreaHight");

                var boardX = (int)boardXProperty.GetValue(validationContext.ObjectInstance, null);
                var goalAreaHight = (int)goalAreaHightProperty.GetValue(validationContext.ObjectInstance, null);

                if ((int)value > boardX * goalAreaHight)
                {
                    return new ValidationResult("Cannot place that many goals in the Goals Area!");
                }

                return null;
            }
        }

        public class PlayersNumberAttribute : ValidationAttribute
        {
            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                var boardXProperty = validationContext.ObjectType.GetProperty("boardX");
                var goalAreaHightProperty = validationContext.ObjectType.GetProperty("goalAreaHight");

                var boardX = (int)boardXProperty.GetValue(validationContext.ObjectInstance, null);
                var goalAreaHight = (int)goalAreaHightProperty.GetValue(validationContext.ObjectInstance, null);

                if ((int)value > boardX * goalAreaHight)
                {
                    return new ValidationResult("Cannot place that many players in the Goals Area!");
                }

                return null;
            }
        }

        public class GoalAreaHeightAttribute : ValidationAttribute
        {
            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                var boardYProperty = validationContext.ObjectType.GetProperty("boardY");
                var boardY = (int)boardYProperty.GetValue(validationContext.ObjectInstance, null);

                if (2 * (int)value >= boardY)
                {
                    return new ValidationResult("The Goal Area is too big compared to the height of the board!");
                }

                return null;
            }
        }

        public class ShamPieceProbabilityAttribute : ValidationAttribute
        {
            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                if ((double)value <= 0 || (double)value >= 1)
                {
                    return new ValidationResult("Sham piece probability should be bigger than 0 and smaller than 1!");
                }

                return null;
            }
        }
    }
}
