﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communicating.MessageTypes
{
    public enum GameMasterMessageType
    {
        IsSham = 101, // Odpowied´z na sprawdzenie fikcyjnosci ´
        DestroyPiece = 102, // Odpowied´z na prosb˛e o zniszczenie fragmentu ´
        Discovery = 103, // Odpowied´z na akcj˛e discovery
        EndGame = 104, // Wiadomos´c o zako ´ nczeniu gry ´
        StartGame = 105, // Wiadomos´c o rozpocz˛eciu gry ´
        AnswerCommunication = 106, // Wiadomos´c przekazuj ˛aca zapytanie o wymian˛e informacji do adresata ´
        Join = 107, // Odpowied´z na zapytanie o doł ˛aczenie
        Move = 108, // Odpowied´z na zapytanie o ruch
        PickUpPiece = 109, // Odpowied´z na podniesienie kawałka
        PutPiece = 110, // Odpowied´z na polozenie kawałka
        ForwardAnswerCommunication = 111
    }
}
