﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communicating.MessageTypes
{
    public enum ErrorMessageType
    {
        Move = 901, // Błędny ruch
        PickUpPiece = 902, // Błędne odłożenie kawałka
        PutPiece = 903, // Błędne położenie kawałka
        Locked = 904, // Nieodczekanie kary
        Undefined = 905 // Niezdefiniowany błąd
    }
}
