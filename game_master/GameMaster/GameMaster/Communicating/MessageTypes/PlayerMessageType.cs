﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communicating.MessageTypes
{
    public enum PlayerMessageType
    {
        IsSham = 001, // Zapytanie czy trzymany fragment jest fikcyjny
        DestroyPiece = 002, // Zapytanie o zniszczenie fragmentu
        Discovery = 003, // Zapytanie o akcj˛e odkrycia
        AnswerCommunication = 004, // Odpowied´z na wymian˛e informacji
        Communicate = 005, // Zapytanie o wymian˛e informacji
        Join = 006, // Zapytanie o doł ˛aczenie do rozgrywki
        Move = 007, // Zapytanie o ruch
        PickupPiece = 008, // Zapytanie o podniesienie fragmentu
        PutPiece = 009 // Zapytanie o poło˙zenie fragmentu
    }
}
