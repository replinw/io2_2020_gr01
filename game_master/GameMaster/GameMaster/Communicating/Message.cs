﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace GameMaster.Communicating
{
    public class Message
    {
        public int messageID { get; set; }
        public int correlationID { get; set; }
        public int agentID { get; set; }
        public object payload { get; set; }
    }
}