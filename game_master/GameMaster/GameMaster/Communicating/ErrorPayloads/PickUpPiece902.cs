﻿using GameMaster.Gameplay;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communicating.ErrorPayloads
{
    public class PickUpPiece902
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public PickUpPieceErrorType errorSubtype { get; set; }
    }

    public enum PickUpPieceErrorType
    {
        NothingThere,
        Other
    }
}
