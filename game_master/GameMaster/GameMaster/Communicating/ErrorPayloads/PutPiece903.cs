﻿using GameMaster.Gameplay;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communicating.ErrorPayloads
{
    public class PutPiece903
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public PutPieceErrorType errorSubtype { get; set; }
    }

    public enum PutPieceErrorType
    {
        AgentNotHolding,
        CannotPutThere,
        Other
    }
}
