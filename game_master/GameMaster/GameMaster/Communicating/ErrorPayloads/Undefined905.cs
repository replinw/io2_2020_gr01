﻿using GameMaster.Gameplay;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communicating.ErrorPayloads
{
    public class Undefined905
    {
        public Position position { get; set; }
        public bool holdingPiece { get; set; }
    }
}
