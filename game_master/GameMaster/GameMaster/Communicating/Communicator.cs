﻿using GameMaster.Communicating.MessageTypes;
using GameMaster.Gameplay;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using GameMaster.Configurating;
using GameMaster.Communicating.ErrorPayloads;
using System.Threading.Tasks;

namespace GameMaster.Communicating
{
    public class Communicator
    {
        private readonly int communicationServerPort;
        private static readonly int maxMessageSize = 8192;
        private static readonly int messageSizeBytesCount = 2;
        private NetworkStream stream;
        private readonly GM GameMaster;

        public Communicator(int _communicationServerPort, GM _GameMaster)
        {
            GameMaster = _GameMaster;
            communicationServerPort = _communicationServerPort;
        }

        public void Close()
        {
            GameMaster.Log("Closing Communicator");
            stream.Close();
        }

        public void SetStream(NetworkStream networkStream)
        {
            GameMaster.Log("Setting stream to CS");
            stream = networkStream;
        }

        public async Task AwaitForPlayerRequests(List<PlayerMessageType> messageTypesToHandle, Func<bool> endFucntion)
        {
            while (!endFucntion())
            {
                try
                {
                    Message message;
                    byte[] buffer;

                    var messageSizeBuffer = new byte[messageSizeBytesCount];
                    await stream.ReadAsync(messageSizeBuffer, 0, messageSizeBytesCount);
                    var messageSize = BitConverter.ToInt16(messageSizeBuffer);

                    if (messageSize > maxMessageSize)
                    {
                        var logMessage = $"Max message size is ${maxMessageSize} bytes and incoming message size is {messageSize} bytes.";
                        GameMaster.Log(logMessage);
                        continue;
                    }

                    buffer = new byte[messageSize];
                    await stream.ReadAsync(buffer, 0, messageSize);

                    string jsonStr = Encoding.UTF8.GetString(buffer);
                    try
                    {
                        message = JsonConvert.DeserializeObject<Message>(jsonStr);
                    }
                    catch (JsonException e)
                    {
                        var logMessage = $"{DateTime.Now}: Error in incoming JSON message: " + e.Message;
                        GameMaster.Log(logMessage);
                        GameMaster.Log(jsonStr);

                        continue;
                    }
                    if (messageTypesToHandle.Select(x => (int)x).Contains(message.messageID))
                    {
                        GameMaster.Log($"Message {message.messageID} from {message.agentID}");
                        switch (message.messageID)
                        {
                            case (int)PlayerMessageType.IsSham:
                                await HandleIsShamMessage(message);
                                break;
                            case (int)PlayerMessageType.DestroyPiece:
                                await HandleDestroyPieceMessage(message);
                                break;
                            case (int)PlayerMessageType.Discovery:
                                await HandleDiscoveryMessage(message);
                                break;
                            case (int)PlayerMessageType.AnswerCommunication:
                                await HandleAnswerCommunicationMessage(message);
                                break;
                            case (int)PlayerMessageType.Communicate:
                                await HandleCommunicateMessage(message);
                                break;
                            case (int)PlayerMessageType.Join:
                                await HandleJoinMessage(message);
                                break;
                            case (int)PlayerMessageType.Move:
                                await HandleMoveMessage(message);
                                break;
                            case (int)PlayerMessageType.PickupPiece:
                                await HandlePickUpPieceMessage(message);
                                break;
                            case (int)PlayerMessageType.PutPiece:
                                await HandlePutPieceMessage(message);
                                break;
                        }
                    }
                    else
                    {
                        await HandleUndefinedMessage(message);
                    }
                }
                catch (IOException exception)
                {
                    var logMessage = $"{DateTime.Now}: Connection with the Communication Server dropped." + Environment.NewLine + exception.Message;
                    GameMaster.Log(logMessage);
                    return;
                }
            }

            return;
        }

        private async Task SendMessage(Message message, JsonSerializerSettings settings = null)
        {
            var messageJson = JsonConvert.SerializeObject(message, settings);
            var messageBytes = Encoding.UTF8.GetBytes(messageJson);

            var messageSize = Convert.ToInt16(messageBytes.Length);
            var messageSizeBytes = BitConverter.GetBytes(messageSize);

            var jointMessage = new byte[messageSize + 2];
            messageSizeBytes.CopyTo(jointMessage, 0);
            messageBytes.CopyTo(jointMessage, 2);

            if (messageSize > maxMessageSize)
            {
                var logMessage = $"Max message size is ${maxMessageSize} bytes and outgoing message size is {messageSize} bytes.";
                GameMaster.Log(logMessage);
                return;
            }

            GameMaster.Log($"Sending message {message.messageID} to agent {message.agentID}");
            
            try
            {
                await stream.WriteAsync(jointMessage);
            }
            catch
            {
                GameMaster.Log($"Failed to send message {message.messageID} to agent {message.agentID}");
            }
        }

        private async Task HandleJoinMessage(Message message)
        {
            var accepted = false;
            var payload = (message.payload as JObject).ToObject<PlayerPayloads.Join006>();
            var team = payload.teamID;

            accepted = GameMaster.AddPlayer(message.agentID, team);

            var response = new Message()
            {
                agentID = message.agentID,
                messageID = 107,
                correlationID = message.correlationID,
                payload = new GameMasterPayloads.Join107()
                {
                    accepted = accepted,
                    agentID = message.agentID
                }
            };

            await SendMessage(response);
        }

        public async Task SendStartGameMesseges()
        {
            foreach (var player in GameMaster.Players)
                await SendMessage(PrepareStartGameMessage(player.Value));
        }

        public async Task SendEndGameMessage(Team winner)
        {
            foreach (var player in GameMaster.Players)
                await SendMessage(PrepareEndGameMessage(player.Value, winner));
        }

        private Message PrepareStartGameMessage(Player player)
        {
            return new Message()
            {
                agentID = player.id,
                messageID = (int)GameMasterMessageType.StartGame,
                payload = new GameMasterPayloads.StartGame105()
                {
                    agentID = player.id,
                    alliesIDs = GameMaster.Players.Where(x => x.Key != player.id && x.Value.team == player.team).Select(x => x.Key).ToArray(),
                    teamID = player.team,
                    boardSize = new Position() { x = GameMaster.configuration.boardX, y = GameMaster.configuration.boardY },
                    enemiesIDs = GameMaster.Players.Where(x => x.Value.team != player.team).Select(x => x.Key).ToArray(),
                    goalAreaSize = GameMaster.configuration.goalAreaHight,
                    leaderID = GameMaster.Players.Where(x => x.Value.team == player.team).Single(x => x.Value.isLeader).Key,
                    numberOfGoals = GameMaster.configuration.numberOfGoals,
                    numberOfPieces = GameMaster.configuration.numberOfPieces,
                    numberOfPlayers = new GameMasterPayloads.NumberOfPlayers() { allies = GameMaster.configuration.teamSize, enemies = GameMaster.configuration.teamSize },
                    penalties = new GameMasterPayloads.Penalties()
                    {
                        checkForSham = GameMaster.configuration.checkForShamPenalty,
                        destroyPiece = GameMaster.configuration.destroyPiecePenalty,
                        discovery = GameMaster.configuration.discoveryPenalty,
                        informationExchange = GameMaster.configuration.informationExchangePenalty,
                        move = GameMaster.configuration.movePenalty,
                        putPiece = GameMaster.configuration.putPenalty
                    },
                    position = new Position() { x = player.position.x, y = player.position.y },
                    shamPieceProbability = GameMaster.configuration.shamPieceProbability
                }
            };
        }

        private Message PrepareEndGameMessage(Player player, Team winner)
        {
            return new Message()
            {
                agentID = player.id,
                messageID = (int)GameMasterMessageType.EndGame,
                payload = new GameMasterPayloads.EndGame104()
                {
                    winner = winner
                }
            };
        }

        private Message PrepareLockedMessage(Message message, Player player)
        {
            return new Message()
            {
                agentID = message.agentID,
                messageID = (int)ErrorMessageType.Locked,
                correlationID = message.correlationID,
                payload = new ErrorPayloads.Locked904()
                {
                    delay = (int)(player.LockedTill - DateTime.Now).TotalMilliseconds
                }
            };
        }

        private async Task HandleUndefinedMessage(Message message)
        {
            if (GameMaster.Players.ContainsKey(message.agentID))
            {
                var player = GameMaster.Players[message.agentID];

                var response = new Message()
                {
                    agentID = message.agentID,
                    messageID = (int)ErrorMessageType.Undefined,
                    correlationID = message.correlationID,
                    payload = new ErrorPayloads.Undefined905()
                    {
                        position = new Position() { x = player.position.x, y = player.position.y },
                        holdingPiece = player.holding == null
                    }
                };

                await SendMessage(response);
            }
        }

        private async Task HandlePutPieceMessage(Message message)
        {
            var player = GameMaster.Players[message.agentID];
            Message response;

            if (player.holding == null)
            {
                response = new Message()
                {
                    agentID = message.agentID,
                    messageID = (int)ErrorMessageType.PutPiece,
                    correlationID = message.correlationID,
                    payload = new ErrorPayloads.PutPiece903()
                    {
                        errorSubtype = PutPieceErrorType.AgentNotHolding
                    }
                };
            }
            else
            {
                response = new Message()
                {
                    agentID = message.agentID,
                    messageID = (int)GameMasterMessageType.PutPiece,
                    correlationID = message.correlationID,
                    payload = new GameMasterPayloads.PutPiece110()
                    {
                        putInformation = player.Put()
                    }
                };
            }

            await SendMessage(response);
        }

        private async Task HandlePickUpPieceMessage(Message message)
        {
            var player = GameMaster.Players[message.agentID];
            Message response;

            if (player.position.pieces.Count == 0)
            {
                response = new Message()
                {
                    agentID = message.agentID,
                    messageID = (int)ErrorMessageType.PickUpPiece,
                    correlationID = message.correlationID,
                    payload = new ErrorPayloads.PickUpPiece902()
                    {
                        errorSubtype = PickUpPieceErrorType.NothingThere
                    }
                };

            }
            else
            {
                player.position.PickUp(player);
                response = new Message()
                {
                    agentID = message.agentID,
                    messageID = (int)GameMasterMessageType.PickUpPiece,
                    correlationID = message.correlationID,
                    payload = new GameMasterPayloads.PickUpPiece109()
                    {
                    }
                };
            }

            await SendMessage(response);
        }

        private async Task HandleMoveMessage(Message message)
        {
            var player = GameMaster.Players[message.agentID];
            Message response;
            if (player.TryLock(GameMaster.configuration.movePenalty))
            {
                response = PrepareLockedMessage(message, player);
            }
            else
            {
                var payload = (message.payload as JObject).ToObject<PlayerPayloads.Move007>();
                var direction = payload.direction;

                bool playermoved = GameMaster.MovePlayerIfPossible(player, direction);

                response = new Message()
                {
                    agentID = message.agentID,
                    messageID = (int)GameMasterMessageType.Move,
                    correlationID = message.correlationID,
                    payload = new GameMasterPayloads.Move108()
                    {
                        madeMove = playermoved,
                        closestPiece = Program.GameMaster.CalculateClosestPiece(player.position.x, player.position.y),
                        currentPosition = new Position() { x = player.position.x, y = player.position.y },
                    }
                };
            }

            await SendMessage(response);
        }

        private async Task HandleCommunicateMessage(Message message)//wysyøamy czy ktos jest liderem
        {
            var player = GameMaster.Players[message.agentID];
            Message response;

            if (!player.TryLock(GameMaster.configuration.informationExchangePenalty))
            {
              
                response = new Message()
                {
                    agentID = message.agentID,
                    messageID = (int)GameMasterMessageType.AnswerCommunication,
                    correlationID = message.correlationID,
                    payload = new GameMasterPayloads.AnswerCommunication106()
                    {
                        askingID = message.agentID,
                        leader = player.isLeader,
                        teamID = player.team
                    }
                };

            }
            else
            {
                response = PrepareLockedMessage(message, player);
            }

            await SendMessage(response);
        }

        private async Task HandleAnswerCommunicationMessage(Message message)
        {
            var player = GameMaster.Players[message.agentID];
            Message response;

            if (!player.TryLock(GameMaster.configuration.informationExchangePenalty))
            {           
                response = new Message()
                {
                    agentID = message.agentID,
                    messageID = (int)GameMasterMessageType.ForwardAnswerCommunication,
                    correlationID = message.correlationID,
                    payload = message.payload
                };

            }
            else
            {
                response = PrepareLockedMessage(message, player);
            }

            await SendMessage(response);
        }

        private async Task HandleDiscoveryMessage(Message message)
        {
            var player = GameMaster.Players[message.agentID];
            Message response;
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };

            if (!player.TryLock(GameMaster.configuration.discoveryPenalty))
            {
                response = new Message()
                {
                    agentID = message.agentID,
                    messageID = (int)GameMasterMessageType.Discovery,
                    correlationID = message.correlationID,
                    payload = new GameMasterPayloads.Discovery103()
                    {
                        distanceN = GameMaster.CalculateClosestPiece(player.position.x, player.position.y + 1),
                        distanceNE = GameMaster.CalculateClosestPiece(player.position.x + 1, player.position.y + 1),
                        distanceE = GameMaster.CalculateClosestPiece(player.position.x + 1, player.position.y),
                        distanceSE = GameMaster.CalculateClosestPiece(player.position.x + 1, player.position.y - 1),
                        distanceS = GameMaster.CalculateClosestPiece(player.position.x, player.position.y - 1),
                        distanceSW = GameMaster.CalculateClosestPiece(player.position.x - 1, player.position.y - 1),
                        distanceW = GameMaster.CalculateClosestPiece(player.position.x - 1, player.position.y),
                        distanceNW = GameMaster.CalculateClosestPiece(player.position.x - 1, player.position.y + 1),
                        distanceFromCurrent = GameMaster.CalculateClosestPiece(player.position.x, player.position.y),
                    }
                };
            }
            else
            {
                response = PrepareLockedMessage(message, player);
            }

            await SendMessage(response, settings);
        }

        private async Task HandleDestroyPieceMessage(Message message)
        {        
            var player = GameMaster.Players[message.agentID];
            Message response;

            if (!player.TryLock(GameMaster.configuration.destroyPiecePenalty))
            {
                player.DestroyHolding();

                response = new Message()
                {
                    agentID = message.agentID,
                    messageID = (int)GameMasterMessageType.DestroyPiece,
                    correlationID = message.correlationID,
                    payload = new GameMasterPayloads.DestroyPiece102()
                    {
                    }
                };
            }
            else
            {
                response = PrepareLockedMessage(message, player);
            }

            await SendMessage(response);
        }

        private async Task HandleIsShamMessage(Message message)
        {
            var player = GameMaster.Players[message.agentID];
            Message response;

            if (!player.TryLock(GameMaster.configuration.checkForShamPenalty))
            {
                var isSham = player.CheckHolding();

                if (isSham == null)
                {
                    response = new Message()
                    {
                        agentID = message.agentID,
                        messageID = (int)ErrorMessageType.Undefined,
                        correlationID = message.correlationID,
                        payload = new ErrorPayloads.Undefined905()
                        {
                            holdingPiece = false,
                            position = new Position() { x = player.position.x, y = player.position.y }
                        }
                    };
                }
                else
                {
                    response = new Message()
                    {
                        agentID = message.agentID,
                        messageID = (int)GameMasterMessageType.IsSham,
                        correlationID = message.correlationID,
                        payload = new GameMasterPayloads.IsSham101()
                        {
                            sham = isSham.Value
                        }
                    };
                }
            }
            else
            {
                response = PrepareLockedMessage(message, player);
            }

            await SendMessage(response);
        }
    }
}
