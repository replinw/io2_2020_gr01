﻿using GameMaster.Gameplay;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communicating.GameMasterPayloads
{
    public class StartGame105
    {
        public int agentID { get; set; }
        public int[] alliesIDs { get; set; }
        public int leaderID { get; set; }
        public int[] enemiesIDs { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public Team teamID { get; set; }
        public Position boardSize { get; set; }
        public int goalAreaSize { get; set; }
        public NumberOfPlayers numberOfPlayers { get; set; }
        public int numberOfPieces { get; set; }
        public int numberOfGoals { get; set; }
        public Penalties penalties { get; set; }
        public double shamPieceProbability { get; set; }
        public Position position { get; set; }
    }

    public class Penalties
    {
        public int move { get; set; }
        public int checkForSham { get; set; }
        public int discovery { get; set; }
        public int destroyPiece { get; set; }
        public int putPiece { get; set; }
        public int informationExchange { get; set; }
    }

    public class NumberOfPlayers
    {
        public int allies { get; set; }
        public int enemies { get; set; }
    }
}
