﻿using GameMaster.Gameplay;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communicating.GameMasterPayloads
{
    public class EndGame104
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public Team winner { get; set; }
    }
}
