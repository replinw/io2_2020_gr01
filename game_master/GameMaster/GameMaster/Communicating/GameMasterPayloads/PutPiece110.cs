﻿using GameMaster.Gameplay;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace GameMaster.Communicating.GameMasterPayloads
{
    public class PutPiece110
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public PutInformation putInformation { get; set; }
    }
    public enum PutInformation
    {
        NormalOnGoalField,// Zwracane na położenie normal piece na goal
        NormalOnNonGoalField,// Zwracane na położenie normal piece na nongoal
        TaskField,// Zwracane na położenie normal albo sham na taskField
        SchamOnGoalArea //Zwracane na położenie sham w goal area
    }

}
