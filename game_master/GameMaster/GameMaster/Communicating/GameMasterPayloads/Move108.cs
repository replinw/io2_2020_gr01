﻿using GameMaster.Gameplay;
using GameMaster.GamePlay;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communicating.GameMasterPayloads
{
    public class Move108
    {
        public bool madeMove { get; set; }
        public Position currentPosition { get; set; }
        public int? closestPiece { get; set; }
    }
}
