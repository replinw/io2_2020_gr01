﻿using GameMaster.Gameplay;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communicating.GameMasterPayloads
{
    public class AnswerCommunication106
    {
        public int askingID { get; set; }
        public bool leader { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public Team teamID { get; set; }
    }
}
