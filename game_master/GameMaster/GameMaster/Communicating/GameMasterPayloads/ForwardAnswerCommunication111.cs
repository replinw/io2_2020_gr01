﻿using GameMaster.Communicating.PlayerPayloads;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace GameMaster.Communicating.GameMasterPayloads
{
    public class ForwardAnswerCommunication111
    {
        public int respondToID { get; set; }//todo tu jest w dokumentacji respondingID gdyby byøo  jak w 004 to niepotrzebna nam ta wiadomosc
        public int[] distances { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public goalInfo[] redTeamGoalAreaInformations { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public goalInfo[] blueTeamGoalAreaInformations { get; set; }
    }
}
