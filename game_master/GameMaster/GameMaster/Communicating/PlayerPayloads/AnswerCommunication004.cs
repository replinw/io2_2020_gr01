﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communicating.PlayerPayloads
{
    public class AnswerCommunication004
    {
        public int respondToID { get; set; }
        public int[] distances { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public goalInfo[] redTeamGoalAreaInformations { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public goalInfo[] blueTeamGoalAreaInformations { get; set; }
    }

    public enum goalInfo
    {
        IDK,
        N,
        G
    }
}
