﻿using GameMaster.GamePlay;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communicating.PlayerPayloads
{
    public class Move007
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public Direction direction { get; set; }
    }
}
