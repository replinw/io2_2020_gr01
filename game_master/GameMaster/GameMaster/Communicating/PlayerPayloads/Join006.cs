﻿using GameMaster.Gameplay;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Communicating.PlayerPayloads
{
    public class Join006
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public Team teamID { get; set; }
    }
}
