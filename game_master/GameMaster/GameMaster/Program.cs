﻿using System;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Logging.Serilog;
using Avalonia.ReactiveUI;
using GameMaster.Communicating;
using GameMaster.Configurating;
using GameMaster.Gameplay;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace GameMaster
{
    class Program
    {
        public static GM GameMaster = new GM();

        public static void Main(string[] args)
        {
            if (args.Contains("--no-gui") || args.Contains("-n"))
                MainNoGUI()
                    .Wait();
            else
                BuildAvaloniaApp()
                    .StartWithClassicDesktopLifetime(args);
        }

        // Avalonia configuration, don't remove; also used by visual designer.
        public static AppBuilder BuildAvaloniaApp()
            => AppBuilder.Configure<App>()
                .UsePlatformDetect()
                .LogToDebug()
                .UseReactiveUI();

        public async static Task MainNoGUI()
        {
            await GameMaster.ReadConfigurationFile();
            if (!GameMaster.ValidateConfiguration())
                throw new ArgumentException("Invalid configuration.");

            if (!await GameMaster.ConnectToCommunicationServer())
                throw new InvalidOperationException("Could not connect to Communication Server. Verify if you provided proper IP and port!");

            await GameMaster.AwaitForPlayersToJoin();

            GameMaster.InitializeGame();

            await GameMaster.StartGame();

            await GameMaster.AwaitForPlayerRequests();
        }
    }
}

