﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Gameplay
{
    public class Position
    {
        public int x { get; set; }
        public int y { get; set; }
    }
}
