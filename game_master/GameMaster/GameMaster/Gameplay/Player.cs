﻿using Avalonia.Media;
using GameMaster.Communicating.GameMasterPayloads;
using GameMaster.Gameplay.Field;
using GameMaster.Gameplay.Piece;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Gameplay
{
    public class Player
    {
        public int id { get; set; }
        public Team team { get; set; }
        public bool isLeader { get; set; }
        public AbstractPiece holding { get; set; }
        public AbstractField position { get; set; }
        public DateTime LockedTill { get; set; }

        public (string, Brush) DrawPlayer()
        {
            if (team == Team.red)
            {
                if (holding != null)
                    return ("RP", new SolidColorBrush(Color.FromRgb(255, 102, 102)));
                else
                    return ("P", new SolidColorBrush(Color.FromRgb(255, 102, 102)));
            }
            else
            {
                if (holding != null)
                    return ("BP", new SolidColorBrush(Color.FromRgb(102, 255, 255)));
                else
                    return ("B", new SolidColorBrush(Color.FromRgb(102, 255, 255)));
            }
        }

        public bool TryLock(int milliSeconds) // Zwraca false jesli nie jest zablokowany
        {
            if (LockedTill <= DateTime.Now)
            {
                LockedTill = DateTime.Now + TimeSpan.FromMilliseconds(milliSeconds);
                return false;
            }
            else
                return true;
        }

        public void DestroyHolding() // Zawsze niszczy, losuje nowy
        {
            if (holding != null)
            {
                holding = null;
                Program.GameMaster.GeneratePiece();
            }
        }

        public bool? CheckHolding() // Sprawdza czy jest shamem, jesli tak niszczy
        {
            if (holding == null)
                return null;

            if (holding.CheckForSham())
            {
                DestroyHolding();
                return true;
            }
            return false;
        }

        public void SetHolding(AbstractPiece piece)
        {
            if (holding == null)
                holding = piece;
        }

        public bool Move(AbstractField field) // Metoda poruszajaca gracza na wskazane pole. Korzysta z metody obiektu postion. zwraca czy sie poruszyl
        {
            return field.MoveHere(this);
        }

        public PutInformation Put()
        {
            PutInformation getPoints = holding.Put(position);
            holding = null;
            return getPoints;
        }

        private int[] GetPosition()
        {
            return new int[2] { position.x, position.y };
        }
    }
}
