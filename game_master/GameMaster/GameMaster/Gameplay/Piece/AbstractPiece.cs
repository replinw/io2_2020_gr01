﻿using Avalonia.Media;
using GameMaster.Communicating.GameMasterPayloads;
using GameMaster.Gameplay.Field;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Gameplay.Piece
{
    public abstract class AbstractPiece
    {
        public abstract bool CheckForSham();
        public abstract PutInformation Put(AbstractField abstractField);
        public abstract (string, Brush) DrawPiece();

    }
}
