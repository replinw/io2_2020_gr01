﻿using Avalonia.Media;
using GameMaster.Communicating.GameMasterPayloads;
using GameMaster.Gameplay.Field;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Gameplay.Piece
{
    public class ShamPiece : AbstractPiece
    {
        public override bool CheckForSham()
        {
            return true;
        }

        public override (string, Brush) DrawPiece()
        {
            return ("S", new SolidColorBrush(Color.FromRgb(102, 255, 140)));//green
        }

        public override PutInformation Put(AbstractField abstractField)
        {
            return abstractField.PutSham(this);
        }
    }
}
