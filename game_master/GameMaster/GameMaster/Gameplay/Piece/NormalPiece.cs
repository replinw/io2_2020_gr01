﻿using Avalonia.Media;
using GameMaster.Communicating.GameMasterPayloads;
using GameMaster.Gameplay.Field;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Gameplay.Piece
{
    public class NormalPiece : AbstractPiece
    {

        public override bool CheckForSham()
        {
            return false;
        }

        public override (string, Brush) DrawPiece()
        {
            return ("P", new SolidColorBrush(Color.FromRgb(179, 255, 102)));//green
        }

        public override PutInformation Put(AbstractField abstractField)
        {
            return abstractField.Put(this);
        }
    }
}
