﻿using Avalonia.Media;
using GameMaster.Communicating.GameMasterPayloads;
using GameMaster.Gameplay.Piece;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Gameplay.Field
{
    public class NonGoalField : AbstractField
    {
        public NonGoalField(int y, int x, Team team) : base(y, x)
        {
            this.team = team;
        }

        private readonly Team team;

        public override (string,Brush) DrawField()
        {
            if (isAchieved)
                return ("N", new SolidColorBrush(Color.FromRgb(179, 102, 255)));//violet
            else
                return (null,null);
        }

        public override void PickUp(Player player)//nie mozna podnosic z goal area
        {
            
        }

        public override PutInformation Put(AbstractPiece piece)
        {
            this.pieces.Add(piece);
            Program.GameMaster.GeneratePiece();

            if (pieces.Count == 1)
                isAchieved = true;

            return PutInformation.NormalOnNonGoalField;
        }

        public override PutInformation PutSham(AbstractPiece piece)
        {
            this.pieces.Add(piece);
            Program.GameMaster.GeneratePiece();
            return PutInformation.SchamOnGoalArea;
        }
    }
}
