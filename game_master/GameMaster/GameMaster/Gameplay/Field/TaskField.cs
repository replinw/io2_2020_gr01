﻿using Avalonia.Media;
using GameMaster.Communicating.GameMasterPayloads;
using GameMaster.Gameplay.Piece;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Gameplay.Field
{
    public class TaskField : AbstractField
    {
        public TaskField(int y, int x) : base(y, x) { }

        public override (string,Brush) DrawField()
        {
            return (null,null);
        }

        public override void PickUp(Player player)
        {          
            player.holding = pieces[pieces.Count-1];
            pieces.RemoveAt(pieces.Count-1);
        }

        public override PutInformation Put(AbstractPiece piece)
        {
            this.pieces.Add(piece);
            return PutInformation.TaskField;
        }

        public override PutInformation PutSham(AbstractPiece piece)
        {
            this.pieces.Add(piece);
            return PutInformation.TaskField;
        }
    }
}
