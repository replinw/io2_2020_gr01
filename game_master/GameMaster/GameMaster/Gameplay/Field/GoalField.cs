﻿using Avalonia.Media;
using GameMaster.Communicating.GameMasterPayloads;
using GameMaster.Gameplay.Piece;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Gameplay.Field
{
    public class GoalField : AbstractField
    {
        public GoalField(int y, int x, Team team) : base(y, x)
        {
            this.team = team;
        }

        private readonly Team team;

        public override (string,Brush) DrawField()
        {
            if (isAchieved)
                return ("YG", new SolidColorBrush(Color.FromRgb(255, 179, 102))); //orange
            else
                return ("G", new SolidColorBrush(Color.FromRgb(255, 255, 102))); //yellow
        }

        public override void PickUp(Player player) // Nie mozna podnosic z goalarea
        {
            
        }

        public override PutInformation Put(AbstractPiece piece)
        {
            this.pieces.Add(piece);
            Program.GameMaster.GeneratePiece();

            if (this.pieces.Count == 1) // Odłozenie przyznaje punkty, jesli wczesniej na te pole nie był odłozony inny fragment
            {
                isAchieved = true;

                if (team == Team.red)
                    Program.GameMaster.RedTeamPoints++;
                else
                    Program.GameMaster.BlueTeamPoints++;

                return PutInformation.NormalOnGoalField;
            }
            else return PutInformation.NormalOnNonGoalField;
         }

        public override PutInformation PutSham(AbstractPiece piece)
        {
            this.pieces.Add(piece);
            Program.GameMaster.GeneratePiece();
            return PutInformation.SchamOnGoalArea;
        }
    }
}
