﻿using Avalonia.Media;
using GameMaster.Communicating.GameMasterPayloads;
using GameMaster.Gameplay.Piece;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.Gameplay.Field
{
    public abstract class AbstractField
    {
        public readonly int x;
        public readonly int y;
        public readonly List<AbstractPiece> pieces = new List<AbstractPiece>();

        public bool isAchieved { get; set; }
        public Player WhosHere { get; private set; }

        public abstract void PickUp(Player player);
        public abstract PutInformation Put(AbstractPiece piece);
        public abstract PutInformation PutSham(AbstractPiece piece);
        public abstract (string,Brush) DrawField();

        public AbstractField(int y, int x)
        {
            this.x = x;
            this.y = y;
            this.isAchieved = false;
        }
        
        public void Leave()
        {
            WhosHere = null;
        }


        public bool MoveHere(Player player)
        {
            if (WhosHere == null)
            {
                player.position.Leave();
                player.position = this;
                WhosHere = player;
                return true;
            }

            return false;
        }

        public bool ContainsPieces()
        {
            return pieces.Count != 0;
        }

        public (int, int) GetPosition()
        {
            return (y, x);
        }
    }
}
