﻿using GameMaster.Communicating;
using GameMaster.Communicating.MessageTypes;
using GameMaster.Configurating;
using GameMaster.Gameplay.Field;
using GameMaster.Gameplay.Piece;
using GameMaster.GamePlay;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace GameMaster.Gameplay
{
    public class GM
    {
        public Dictionary<int, Player> Players { get; private set; }
        public AbstractField[,] Board { get; private set; }
        public Configuration configuration { get; private set; }

        public bool GameStarted { get; private set; } = false;

        public int RedTeamPoints = 0;
        public int BlueTeamPoints = 0;

        private readonly Random random;
        public Communicator communicator;
        private TcpClient client;
        public readonly StreamWriter log;
        private bool isConfigurating = true;

        public event EventHandler @endGameEvent;

        public GM(int? seed = null)
        {
            Players = new Dictionary<int, Player>();
            random = seed.HasValue ? new Random(seed.Value) : new Random();
            log = CreateLogFile();
        }

        private StreamWriter CreateLogFile()
        {
            if (!Directory.Exists("Logs"))
            {
                Directory.CreateDirectory("Logs");
            }

            return new StreamWriter($"Logs/Log_{DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss")}_{Guid.NewGuid()}.txt");
        }

        public async Task Close()
        {
            endGameEvent?.Invoke(this, EventArgs.Empty);

            await communicator.SendEndGameMessage(CheckWinner());
            Thread.Sleep(1000);
            communicator.Close();
            client.Close();
            Log($"Closing GameMaster");
            await log.FlushAsync();
            log.Close();
        }

        public async Task ReadConfigurationFile()
        {
            Log($"Reading configuration file");
            configuration = await Configuration.ReadConfigurationFile();
        }

        public bool SetConfiguration(Configuration configuration)
        {
            if (isConfigurating)
            {
                Log($"Setting configuration");
                this.configuration = configuration;
            }

            return isConfigurating;
        }

        public bool ValidateConfiguration()
        {
            return configuration != null && configuration.Validate();
        }

        public void Log(string message)
        {
            log.WriteLine($"{DateTime.Now}: " + message);
        }

        public async Task<bool> ConnectToCommunicationServer()
        {
            client = new TcpClient();

            try
            {
                await client.ConnectAsync(configuration.CsIP, configuration.CsPort);
                Log("Connected to Communication Server");
            }
            catch (SocketException exception)
            {
                Log("Could not connect to the Communication Server: " + exception.Message);
                return false;
            }

            communicator = new Communicator(configuration.CsPort, this);
            communicator.SetStream(client.GetStream());
            return true;
        }

        public bool AddPlayer(int agentID, Team team)
        {
            var accepted = false;

            if (!GameStarted && !Players.ContainsKey(agentID))
            {
                if (Players.Where(x => x.Value.team == team).Count() < configuration.teamSize)
                {
                    accepted = true;
                    var player = new Player()
                    {
                        id = agentID,
                        isLeader = !Players.Where(x => x.Value.team == team).Any(),
                        team = team
                    };
                    Players.Add(agentID, player);
                }
            }

            return accepted;
        }

        public void InitializeGame()
        {
            Log("Initializing game...");

            isConfigurating = false;

            Board = new AbstractField[configuration.boardY, configuration.boardX];

            GenerateGoalArea(configuration.boardY - configuration.goalAreaHight, configuration.boardY - 1, Team.red);
            GenerateGoalArea(0, configuration.goalAreaHight - 1, Team.blue);

            for (int y = configuration.goalAreaHight; y < configuration.boardY - configuration.goalAreaHight; y++)
                for (int x = 0; x < configuration.boardX; x++)
                    Board[y, x] = new TaskField(y, x);

            PositionPlayers(Team.blue);
            PositionPlayers(Team.red);

            for (int i = 0; i < configuration.numberOfPieces; i++)
                GeneratePiece();

            Log("Game initialized");
        }

        public async Task StartGame()
        {
            Log("Starting game");
            GameStarted = true;
            await communicator.SendStartGameMesseges();
        }

        private void GenerateGoalArea(int miny, int maxy, Team team)
        {
            var goalArea = new List<(int, int)>();
            for (int y = miny; y <= maxy; y++)
                for (int x = 0; x < configuration.boardX; x++)
                    goalArea.Add((y, x));

            for (int i = 0; i < configuration.numberOfGoals; i++)
            {
                int index = random.Next(goalArea.Count);
                Board[goalArea[index].Item1, goalArea[index].Item2] = new GoalField(goalArea[index].Item1, goalArea[index].Item2, team);
                goalArea.RemoveAt(index);
            }

            for (int i = 0; i < goalArea.Count; i++)
                Board[goalArea[i].Item1, goalArea[i].Item2] = new NonGoalField(goalArea[i].Item1, goalArea[i].Item2, team);
        }

        private void PositionPlayers(Team team)
        {
            int miny = (team == Team.blue) ? 0 : configuration.boardY - configuration.goalAreaHight;
            int maxy = (team == Team.blue) ? configuration.goalAreaHight - 1 : configuration.boardY - 1;

            var fields = new List<(int, int)>();
            for (int y = miny; y <= maxy; y++)
                for (int x = 0; x < configuration.boardX; x++)
                    fields.Add((y, x));

            foreach (var player in Players.Where(x => x.Value.team == team))
            {
                int index = random.Next(fields.Count);
                player.Value.position = Board[fields[index].Item1, fields[index].Item2];
                Board[fields[index].Item1, fields[index].Item2].MoveHere(player.Value);
                fields.RemoveAt(index);
            }
        }

        public void GeneratePiece()
        {
            int x = random.Next(configuration.boardX);
            int y = random.Next(configuration.goalAreaHight, configuration.boardY - configuration.goalAreaHight);

            AbstractPiece piece;
            if (random.NextDouble() < configuration.shamPieceProbability)
                piece = new ShamPiece();
            else
                piece = new NormalPiece();

            Board[y, x].pieces.Add(piece);
        }

        public async Task AwaitForPlayersToJoin()
        {
            Log("Waiting for players to join");
            await communicator.AwaitForPlayerRequests(
                new List<PlayerMessageType>() { PlayerMessageType.Join },
                () => Players.Count >= configuration.teamSize * 2);
        }

        public async Task AwaitForPlayerRequests()
        {
            Log("Waiting for player requests");
            var requestsList = Enum
                .GetValues(typeof(PlayerMessageType))
                .Cast<PlayerMessageType>()
                .ToList();

            await communicator.AwaitForPlayerRequests(requestsList, () => Program.GameMaster.RedTeamPoints >= Program.GameMaster.configuration.numberOfGoals || Program.GameMaster.BlueTeamPoints >= Program.GameMaster.configuration.numberOfGoals);

            await Close();
        }

        public Team CheckWinner()
        {
            if (RedTeamPoints >= BlueTeamPoints)
                return Team.red;
            else return Team.blue;
        }

        public bool MovePlayerIfPossible(Player player, Direction direction)
        {
            switch (direction)
            {
                case Direction.N:

                    if (player.position.y < configuration.boardY - 1)
                        return player.Move(Board[player.position.y + 1, player.position.x]);
                    else
                        return false;

                case Direction.E:

                    if (player.position.x < configuration.boardX - 1)
                        return player.Move(Board[player.position.y , player.position.x + 1]);
                    else
                        return false;

                case Direction.S:

                    if (player.position.y > 0)
                        return player.Move(Board[player.position.y - 1, player.position.x]);
                    else
                        return false;

                case Direction.W:

                    if (player.position.x > 0)
                        return player.Move(Board[player.position.y, player.position.x - 1]);
                    else
                        return false;
            }
            return false;
        }

        public int? CalculateClosestPiece(int positionX, int positionY)
        {
            if (positionX < 0 || positionX >= configuration.boardX || positionY < 0 || positionY >= configuration.boardY)
                return null;

            int closest = int.MaxValue;

            for (int y = configuration.goalAreaHight; y < configuration.boardY - configuration.goalAreaHight; y++)
                for (int x = 0; x < configuration.boardX; x++)
                {
                    if (Board[y, x].pieces.Count > 0)
                    {
                        var distance = Math.Abs(x - positionX) + Math.Abs(y - positionY);
                        if (distance < closest)
                            closest = distance;
                    }
                }

            if (closest == int.MaxValue)
                return null;

            return closest;
        }
    }
}
