﻿using GameMaster.Configurating;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.ViewModels
{
    public class ConfigurationQuestionViewModel:ViewModelBase
    {
        public string Greeting => "Would you like to import your own configuration?";
    }
}
