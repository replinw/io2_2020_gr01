﻿using System;
using System.Collections.Generic;
using System.Text;
using GameMaster.Gameplay;
using GameMaster.Gameplay.Field;
using GameMaster.Configurating;
using Avalonia.Controls.Shapes;
using Avalonia.Media;
using Avalonia.Controls;
using ReactiveUI;
using Avalonia.Media.Imaging;
using Avalonia.Threading;

namespace GameMaster.ViewModels
{
    public class BoardGameViewModel : ViewModelBase
    {
        public Configuration Configuration { get; set; }

        private int squareWidth;
        private int squareHeight;
        private readonly int windowWidth = 300;
        private readonly int windowHeight = 600;
        Bitmap boardgame;
        public Bitmap Boardgame
        {
            get => boardgame;
            set => this.RaiseAndSetIfChanged(ref boardgame, value);
        }
        string score;
        public string Score
        {
            get => score;
            set => this.RaiseAndSetIfChanged(ref score, value);
        }
        string seconds;
        public string Seconds
        {
            get => seconds;
            set => this.RaiseAndSetIfChanged(ref seconds, value);
        }
        private int secondsINT;
        private DispatcherTimer timer;
        private DispatcherTimer boardtimer;

        public BoardGameViewModel(Configuration configuration)
        {

            Configuration = configuration;
            squareWidth = windowWidth / Configuration.boardX;
            squareHeight = windowHeight / Configuration.boardY;
            secondsINT = 0;

            timer = new DispatcherTimer
            {
                Interval = TimeSpan.FromSeconds(1)
            };
            timer.Tick += timer_Tick;
            timer.Start();
            DrawBoard();

            boardtimer = new DispatcherTimer
            {
                Interval = TimeSpan.FromMilliseconds(60)
            };
            boardtimer.Tick += timer_BoardTick;
            boardtimer.Start();
        }
        void timer_BoardTick(object sender, EventArgs e)
        {
            DrawBoard();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            secondsINT++;
            Seconds = "TIME " + secondsINT.ToString() + " s";
        }


        private void DrawBoard()
        {
            Score = "RED " + Program.GameMaster.RedTeamPoints + " : " + Program.GameMaster.BlueTeamPoints + " BLUE";

            var drawer = new BoardDrawer(squareWidth, squareHeight);
            drawer.DrawGrid();
            drawer.DrawPieces();
            drawer.DrawGoals();
            drawer.DrawPlayers();
            Boardgame = drawer.Render;
        }
    }
    public class BoardDrawer
    {
        private readonly int SquareWidth;
        private readonly int SquareHeight;
        private readonly int BoardWidth;
        private readonly int BoardHeight;
        public RenderTargetBitmap Render { get; }
        private readonly DrawingContext Context;
        private readonly AbstractField[,] BoardGame;

        private readonly Brush blueBrush = new SolidColorBrush(Color.FromRgb(102, 255, 255));
        private readonly Brush redBrush = new SolidColorBrush(Color.FromRgb(255, 102, 102));
        private readonly Brush greenBrush = new SolidColorBrush(Color.FromRgb(179, 255, 102));
        private readonly Brush yellowBrush = new SolidColorBrush(Color.FromRgb(255, 255, 102));
        private readonly Brush violetBrush = new SolidColorBrush(Color.FromRgb(179, 102, 255));
        private readonly Brush blackBrush = new SolidColorBrush(Colors.Black);
        private readonly Brush whiteBrush = new SolidColorBrush(Colors.White);

       


        public BoardDrawer(int squareWidth, int squareHeight)
        {
            this.BoardGame = Program.GameMaster.Board;
            SquareHeight = squareHeight;
            SquareWidth = squareWidth;
            BoardWidth = BoardGame.GetLength(1);
            BoardHeight = BoardGame.GetLength(0);
            Render = new RenderTargetBitmap(new Avalonia.PixelSize(SquareWidth * BoardWidth, SquareHeight * BoardHeight));

            Context = new DrawingContext(Render.CreateDrawingContext(null));
        }
        public void DrawGrid()
        {
            Pen pen = new Pen(whiteBrush, 0.5);
            Pen penThick = new Pen(whiteBrush, 1.5);
            for (int x = 0; x <= BoardWidth; x++)
            {
                Context.DrawLine(pen, new Avalonia.Point(x * SquareWidth, 0),
                                      new Avalonia.Point(x * SquareWidth, SquareHeight * BoardHeight));
            }
            for (int y = 0; y <= BoardHeight; y++)
            {
                if (y == Program.GameMaster.configuration.goalAreaHight || y == (BoardHeight - Program.GameMaster.configuration.goalAreaHight))
                    Context.DrawLine(penThick, new Avalonia.Point(0, y * SquareHeight),
                                          new Avalonia.Point(SquareWidth * BoardWidth, y * SquareHeight));
                else
                    Context.DrawLine(pen, new Avalonia.Point(0, y * SquareHeight),
                              new Avalonia.Point(SquareWidth * BoardWidth, y * SquareHeight));
            }

        }
        public void DrawPlayers()
        {
            foreach (var player in Program.GameMaster.Players)
            {
                var s = player.Value.DrawPlayer();
                DrawRectangle(s.Item2, player.Value.position.y, player.Value.position.x);
                DrawText(s.Item1, blackBrush, player.Value.position.y, player.Value.position.x);
            }
           
        }
        public void DrawPieces()
        {
            for (int y = 0; y < BoardHeight; y++)
            {
                for (int x = 0; x < BoardWidth; x++)
                {
                    if (BoardGame[y, x].pieces.Count != 0)
                    {
                        var s = BoardGame[y, x].pieces[BoardGame[y, x].pieces.Count - 1].DrawPiece();
                        DrawRectangle(s.Item2, y, x);
                        DrawText(s.Item1, blackBrush, y, x);
                    }
                }
            }
        }
        public void DrawGoals()
        {
            for (int y = 0; y < BoardHeight; y++)
            {
                for (int x = 0; x < BoardWidth; x++)
                {
                    var s = BoardGame[y, x].DrawField();
                    if (s.Item1 != null)
                    {
                        DrawRectangle(s.Item2, y, x);
                        DrawText(s.Item1, blackBrush, y, x);
                    }
                }
            }
        }
        private void DrawRectangle(IBrush brush, int y, int x)
        {
            Context.FillRectangle(brush, new Avalonia.Rect( x * SquareWidth + 1,
                                                            y * SquareHeight + 1,
                                                            SquareWidth - 2,
                                                            SquareHeight - 2 ));
        }
        private void DrawText(string text, IBrush brush, int y, int x)
        {
            Avalonia.Point origin = new Avalonia.Point(x * SquareWidth, y * SquareHeight);
            FormattedText formatedText = new FormattedText
            {
                Text = text,
                TextAlignment = TextAlignment.Center,
                Constraint = new Avalonia.Size(SquareWidth, SquareHeight),
                Typeface = new Typeface(Avalonia.Media.FontFamily.Default, SquareHeight / 2)
            };
            Context.DrawText(brush, origin, formatedText);
        }
    }
}


