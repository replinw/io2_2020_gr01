﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.ViewModels
{
    public class StartViewModel: ViewModelBase
    {
        public string Greeting => "Welcome to Project Game!";
    }
}
