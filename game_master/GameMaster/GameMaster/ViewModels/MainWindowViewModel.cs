﻿using Avalonia;
using Avalonia.Controls;
using GameMaster.Configurating;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GameMaster.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        ViewModelBase content;
        public ViewModelBase Content
        {
            get => content;
            private set => this.RaiseAndSetIfChanged(ref content, value);
        }
        public MainWindowViewModel()
        {
            Content = new StartViewModel();
        }
        public async Task Initialize()
        {
            await Program.GameMaster.ReadConfigurationFile();
            Program.GameMaster.ValidateConfiguration();
           
            await GoToConfigurationQuestionView();
        }
        
        public async Task GoToWaitingForPlayersView()
        {
            if (!Program.GameMaster.ValidateConfiguration())
                return;
            Content = new WaitForPlayersViewModel();
            await Program.GameMaster.AwaitForPlayersToJoin();
            Program.GameMaster.InitializeGame();
            await Program.GameMaster.StartGame();

            GoToGameView();
            await Program.GameMaster.AwaitForPlayerRequests();
        }
        public void GoToGameView()
        {          
            Program.GameMaster.endGameEvent 
                 += (object sender, EventArgs args) =>
                 {
                     Avalonia.Threading.Dispatcher.UIThread.Post(GoToStatisticView);
                 };
            Content = new BoardGameViewModel(Program.GameMaster.configuration);
        }
        public async Task GoToConfigurationQuestionView()
        {
            Content = new ConnectingToCsViewModel();
            var connected = await Program.GameMaster.ConnectToCommunicationServer();
            if (!connected)
            {
                Content = new ConnectionQuestionViewModel(Program.GameMaster.configuration);//pytanie o parametry polaczenia
                return;
            }
            else
                Content = new ConfigurationQuestionViewModel();
        }
        public void GoToConfigurationView()
        {
            Content = new ConfigurationViewModel(Program.GameMaster.configuration);
        }
        public async Task EndGame()
        {
            await Program.GameMaster.Close();
            GoToStatisticView();
        }
        public void GoToEndGameViewNodel()
        {
            Content = new StatisticsViewModel(Program.GameMaster.RedTeamPoints, Program.GameMaster.BlueTeamPoints);
        }
        public void GoToStatisticView()
        {
            Content = new StatisticsViewModel(Program.GameMaster.RedTeamPoints, Program.GameMaster.BlueTeamPoints);
        }
    }
}
