﻿using GameMaster.Configurating;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.ViewModels
{
    public class ConfigurationViewModel:ViewModelBase
    {
        public Configuration Configuration { get; set; }
        public string Greeting => "Please, enter your configuration";

        public ConfigurationViewModel(Configuration configuration)
        {
            this.Configuration = configuration;
        }
    }
}
