﻿using Avalonia.Media;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.ViewModels
{
    public class StatisticsViewModel : ViewModelBase
    {
        public StatisticsViewModel(int redPoints, int bluePoints)
        {
            if (bluePoints > redPoints)
            {
                Title = "Blue team wins!";
                Colour = Brush.Parse("#336699");
            }
            else
            {
                Title = "Red team wins!";
                Colour = Brush.Parse("#CC3333");
            }

            Subtitle = $"RED {redPoints} : {bluePoints} BLUE";
        }

        public string Title { get; }
        public string Subtitle { get; }
        public IBrush Colour { get; }
    }
}
