﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.ViewModels
{
    public class WaitForPlayersViewModel : ViewModelBase
    {
        public string Greeting => "Waiting for players!";
    }
}
