﻿using GameMaster.Configurating;
using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.ViewModels
{
    public class ConnectionQuestionViewModel : ViewModelBase
    {
        public Configuration Configuration { get; set; }
        public string Greeting => "The defualt connection failed, please enter connection parameteres";
       
        public ConnectionQuestionViewModel(Configuration configuration)
        {
            this.Configuration = configuration;
        }
    }
}
