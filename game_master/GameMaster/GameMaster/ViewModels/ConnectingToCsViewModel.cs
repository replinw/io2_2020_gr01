﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.ViewModels
{
    public class ConnectingToCsViewModel:ViewModelBase
    {
        public string Greeting => "Connecting to Communication Server";
    }
}
