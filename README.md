# Metodologia
Waterfall ze względu na idealne dopasowanie do specyfikacji projektu.

# Harmonogram i zadania
Taski i zadania są rozpisane na Trello.
Nie ma estymacji zadań, ponieważ Waterfall nie jest metodyką zwinną.
Waterfall nie przewiduje team meetingów lecz mimo to zaplanowany jest około 1 meeting na tydzień.

# Development
Code review: pull request musi być zaakceptowany przez inną osobę.
Podział pracy: zespół podzielony na grupy do każdego z modułów.

# Specyfikacja:
https://github.com/MINI-IO/IO-project-game  
Uwagi zgłoszone w Issues na powyższym repozytorium.

# Game Master
Programiści: Adrianna Klimczak i Marcel Wenka
Technologia: .NET Core 3.1

# Communication Server
Programiści: Piotr Kowalski
Technologia: .NET Core 3.1

# Player
Programiści: Franciszek Jełowicki i Wojciech Replin
Technologia: go 1.14
