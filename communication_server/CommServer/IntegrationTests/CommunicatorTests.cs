﻿using CommServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

[assembly: AssemblyTrait ("Category", "SkipWhenLiveUnitTesting")]
namespace IntegrationTests
{
    public class CommunicatorTests
    {
        [Fact]
        public void Communicator_ShutDown ()
        {
            var mockServer = new TcpListener (IPAddress.Any, 5000);
            mockServer.Start ();
            var comm = new Communicator (2);
            using var gm = new TcpClient ("localhost", 5000);
            var gmClient = mockServer.AcceptTcpClient ();
            comm.SetGMClient (gmClient);
            using var agent = new TcpClient ("localhost", 5000);
            var agentClient = mockServer.AcceptTcpClient ();
            comm.AddAgentClient (agentClient);
            mockServer.Stop ();
            comm.Start ();
            Thread.Sleep (1000); //This hopefully makes all threads in cs block on first Read

            comm.Cancel ();
            comm.WaitForEnd ();
            Assert.Equal (0, agent.GetStream ().Read (new byte [10], 0, 10));

            var correlation = comm.Correlation;
            foreach (var item in correlation)
            {
                Assert.False (item.Value.Connected);
            }
            var tasks = (List<Task>)comm.GetType ().InvokeMember ("tasks", System.Reflection.BindingFlags.GetField | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic, null, comm, null);
            foreach (var item in tasks)
            {
                Assert.True (item.IsCompleted);
            }
        }

        [Fact]
        public void Communicator_MessageFromAgentToGM ()
        {
            var mockServer = new TcpListener (IPAddress.Any, 5000);
            mockServer.Start ();
            var comm = new Communicator (2);
            using var gm = new TcpClient ("localhost", 5000);
            comm.SetGMClient (mockServer.AcceptTcpClient ());
            using var agent = new TcpClient ("localhost", 5000);
            comm.AddAgentClient (mockServer.AcceptTcpClient ());
            mockServer.Stop ();
            var message = "{\"messageID\":100,\"payload\":\"test\"}";
            comm.Start ();

            agent.GetStream ().Write (BitConverter.GetBytes ((Int16)message.Length), 0, 2);
            agent.GetStream ().Write (Encoding.UTF8.GetBytes (message));

            var msgSizeBuf = new byte [2];
            gm.GetStream ().Read (msgSizeBuf, 0, 2);
            var msgSize = BitConverter.ToInt16 (msgSizeBuf);
            var rec = new byte [msgSize];
            gm.GetStream ().Read (rec, 0, msgSize);
            var utf8Reader = new Utf8JsonReader (rec);
            Message messageReceived;
            messageReceived = JsonSerializer.Deserialize<Message> (ref utf8Reader);
            Assert.Equal (100, messageReceived.messageID);
            Assert.Equal ("test", messageReceived.payload.ToString ());
        }

        [Fact]
        public void Communicator_MessageFromGMToAgent ()
        {
            var mockServer = new TcpListener (IPAddress.Any, 5000);
            mockServer.Start ();
            var comm = new Communicator (2);
            using var gm = new TcpClient ("localhost", 5000);
            comm.SetGMClient (mockServer.AcceptTcpClient ());
            using var agent = new TcpClient ("localhost", 5000);
            comm.AddAgentClient (mockServer.AcceptTcpClient ());
            mockServer.Stop ();
            var temp = comm.Correlation;
            var message = $"{{\"messageID\":100,\"agentID\":{temp.First ().Key},\"payload\":\"test\"}}";
            comm.Start ();

            gm.GetStream ().Write (BitConverter.GetBytes ((Int16)message.Length), 0, 2);
            gm.GetStream ().Write (Encoding.UTF8.GetBytes (message));

            var msgSizeBuf = new byte [2];
            agent.GetStream ().Read (msgSizeBuf, 0, 2);
            var msgSize = BitConverter.ToInt16 (msgSizeBuf);
            var rec = new byte [msgSize];
            agent.GetStream ().Read (rec, 0, msgSize);
            var utf8Reader = new Utf8JsonReader (rec);
            Message messageReceived;
            messageReceived = JsonSerializer.Deserialize<Message> (ref utf8Reader);
            Assert.Equal (100, messageReceived.messageID);
            Assert.Equal (temp.First ().Key, messageReceived.agentID);
            Assert.Equal ("test", messageReceived.payload.ToString ());
        }



        [Fact]
        public void Communicator_EndingMessageFromGMToAgent ()
        {
            var mockServer = new TcpListener (IPAddress.Any, 5000);
            mockServer.Start ();
            var comm = new Communicator (2);
            using var gm = new TcpClient ("localhost", 5000);
            comm.SetGMClient (mockServer.AcceptTcpClient ());
            using var agent = new TcpClient ("localhost", 5000);
            comm.AddAgentClient (mockServer.AcceptTcpClient ());
            mockServer.Stop ();
            var temp = comm.Correlation;
            var id = temp.First ().Key;
            var message = $"{{\"messageID\":104,\"agentID\":{id},\"payload\":\"test\"}}";
            comm.Start ();

            gm.GetStream ().Write (BitConverter.GetBytes ((Int16)message.Length), 0, 2);
            gm.GetStream ().Write (Encoding.UTF8.GetBytes (message));

            var msgSizeBuf = new byte [2];
            agent.GetStream ().Read (msgSizeBuf, 0, 2);
            var msgSize = BitConverter.ToInt16 (msgSizeBuf);
            var rec = new byte [msgSize];
            agent.GetStream ().Read (rec, 0, msgSize);
            var utf8Reader = new Utf8JsonReader (rec);
            Message messageReceived;
            messageReceived = JsonSerializer.Deserialize<Message> (ref utf8Reader);
            Assert.Equal (104, messageReceived.messageID);
            Assert.Equal (id, messageReceived.agentID);
            Assert.Equal ("test", messageReceived.payload.ToString ());
            Assert.Equal (0, agent.GetStream ().Read (new byte [10], 0, 10));
        }

        [Fact]
        public void Communicator_CloseOnGMLostConnection ()
        {
            var mockServer = new TcpListener (IPAddress.Any, 5000);
            mockServer.Start ();
            var comm = new Communicator (2);
            using var gm = new TcpClient ("localhost", 5000);
            comm.SetGMClient (mockServer.AcceptTcpClient ());
            using var agent = new TcpClient ("localhost", 5000);
            comm.AddAgentClient (mockServer.AcceptTcpClient ());
            mockServer.Stop ();
            var temp = comm.Correlation;
            comm.Start ();
            Thread.Sleep (2000);

            gm.Close ();

            comm.WaitForEnd ();
            Assert.Equal (0, agent.GetStream ().Read (new byte [10], 0, 10));
        }

        [Fact]
        public void Communicator_CloseOnLastAgentConnectionLost ()
        {
            var mockServer = new TcpListener (IPAddress.Any, 5000);
            mockServer.Start ();
            var comm = new Communicator (2);
            using var gm = new TcpClient ("localhost", 5000);
            comm.SetGMClient (mockServer.AcceptTcpClient ());
            using var agent = new TcpClient ("localhost", 5000);
            comm.AddAgentClient (mockServer.AcceptTcpClient ());
            mockServer.Stop ();
            var temp = comm.Correlation;
            var id = temp.First ().Key;
            var message = $"{{\"messageID\":100,\"agentID\":{id},\"payload\":\"test\"}}";
            typeof (Program).GetProperty ("Communicator", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public).SetValue (null, comm);
            comm.Start ();

            gm.GetStream ().Write (BitConverter.GetBytes ((Int16)message.Length), 0, 2);
            agent.Close ();
            gm.GetStream ().Write (Encoding.UTF8.GetBytes (message));

            Assert.Equal (0, gm.GetStream ().Read (new byte [10], 0, 10));
        }

        [Fact]
        public void Communicator_Main ()
        {
            var server = new TcpListener (IPAddress.Any, 5000);
            var comm = new Communicator ();
            typeof (Program).GetProperty ("Communicator", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public).SetValue (null, comm);
            var temp = comm.Correlation;
            var main = Task.Run (() => Communicator.ComMain (ref server, 5000, 5001, 2));
            var message = $"{{\"messageID\":100,\"payload\":\"test\"}}";

            using var gm = new TcpClient ("localhost", 5000);
            using var agent = new TcpClient ("localhost", 5001);
            agent.GetStream ().Write (BitConverter.GetBytes ((Int16)message.Length), 0, 2);
            agent.GetStream ().Write (Encoding.UTF8.GetBytes (message));

            var buffer = Message.ReadMessageFromStream (gm.GetStream (), out _, out _);
            var m = Message.BytesToMessage (buffer);
            Assert.Equal (100, m.messageID);

            message = $"{{\"messageID\":104,\"agentID\":{comm.Correlation.Keys.First ()}}}";
            gm.GetStream ().Write (BitConverter.GetBytes ((Int16)message.Length), 0, 2);
            gm.GetStream ().Write (Encoding.UTF8.GetBytes (message));

            buffer = Message.ReadMessageFromStream (agent.GetStream (), out _, out _);
            m = Message.BytesToMessage (buffer);
            Assert.Equal (104, m.messageID);

            main.Wait ();
            Assert.Equal (TaskStatus.RanToCompletion, main.Status);
        }
    }
}
