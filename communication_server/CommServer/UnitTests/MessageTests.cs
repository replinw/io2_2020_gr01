﻿using CommServer;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace UnitTests
{
    public class MessageTests
    {
        [Fact]
        public void Message_BytesToMessage_BufferIsNull ()
        {
            var result = Message.BytesToMessage (null);

            Assert.Null (result);
        }

        [Fact]
        public void Message_BytesToMessage_IsNull()
        {
            var message = "incorrect_json";
            var buffer = JsonSerializer.SerializeToUtf8Bytes (message, new JsonSerializerOptions () { WriteIndented = true });

            var result = Message.BytesToMessage (buffer);

            Assert.Null (result);
        }

        [Fact]
        public void Message_BytesToMessage_Success ()
        {
            var message = @"{""messageID"":9,""correlationID"":10,""payload"":{ }}";
            var buffer = Encoding.UTF8.GetBytes (message);

            var result = Message.BytesToMessage (buffer);

            Assert.NotNull (result);
            Assert.Equal (9, result.messageID);
            Assert.Equal (10, result.correlationID);
        }

        [Fact]
        public void Message_ReadMessageFromStream_Success ()
        {
            var server = new TcpListener (IPAddress.Any, 5000);
            server.Start ();
            using var client = new TcpClient ("localhost", 5000);
            var stream = server.AcceptTcpClient ().GetStream ();
            server.Stop ();
            var bytes = Encoding.UTF8.GetBytes (@"{""messageID"":9,""payload"":{ }}");
            client.GetStream ().Write (BitConverter.GetBytes ((Int16)bytes.Length));
            client.GetStream ().Write (bytes);

            var result = Message.ReadMessageFromStream (stream, out _, out _);

            Assert.Equal (bytes, result);
        }

        [Fact]
        public void Message_ReadMessageFromStream_FirstReadAlreadyClosed ()
        {
            var server = new TcpListener (IPAddress.Any, 5000);
            server.Start ();
            using var client = new TcpClient ("localhost", 5000);
            var stream = server.AcceptTcpClient ().GetStream ();
            server.Stop ();
            client.Close ();
            var mockCommunicator = new Mock<Communicator> ();
            
            var result = Message.ReadMessageFromStream (stream, out _, out _);

            mockCommunicator.Verify ();
            Assert.Null (result);
        }

        [Fact]
        public void Message_ReadMessageFromStream_SecondReadAlreadyClosed ()
        {
            var server = new TcpListener (IPAddress.Any, 5000);
            server.Start ();
            using var client = new TcpClient ("localhost", 5000);
            var stream = server.AcceptTcpClient ().GetStream ();
            server.Stop ();
            client.GetStream ().Write (BitConverter.GetBytes (5), 0, 2);
            client.Close ();
            var mockCommunicator = new Mock<Communicator> ();

            var result = Message.ReadMessageFromStream (stream, out _, out _);

            mockCommunicator.Verify ();
            Assert.Null (result);
        }

        [Fact]
        public void Message_ReadMessageFromStream_InterruptedWait ()
        {
            var server = new TcpListener (IPAddress.Any, 5000);
            server.Start ();
            _ = new TcpClient ("localhost", 5000);
            var stream = server.AcceptTcpClient ();
            server.Stop ();
            var mockCommunicator = new Mock<Communicator> ();
            mockCommunicator.Setup (c => c.Cancel ()).Verifiable ();
            typeof (Program).GetProperty ("Communicator", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public).SetValue (null, mockCommunicator.Object);

            Task.Run (() => { Thread.Sleep (500); stream.Close (); });

            var result = Message.ReadMessageFromStream (stream.GetStream (), out _, out _);

            Assert.Null (result);
        }

        [Fact]
        public void Message_WriteMessage_Success ()
        {
            var server = new TcpListener (IPAddress.Any, 5000);
            server.Start ();
            _ = new TcpClient ("localhost", 5000);
            var stream = server.AcceptTcpClient ();
            server.Stop ();
            var message = new Message () { messageID = 100 };
            typeof (Communicator).GetProperty ("GmClient", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public).SetValue (null, new TcpClient ());

            var result = Message.WriteMessage (message, stream);

            Assert.True (result);
        }

        [Fact]
        public void Message_WriteMessage_Disconnected ()
        {
            var server = new TcpListener (IPAddress.Any, 5000);
            server.Start ();
            var client = new TcpClient ("localhost", 5000);
            var stream = server.AcceptTcpClient ();
            server.Stop ();
            client.Close ();
            var message = new Message () { messageID = 100, payload = new { } };
            var mockCommunicator = new Mock<Communicator> ();

            _ = Message.WriteMessage (message, stream, 10);
            var result = Message.WriteMessage (message, stream, 10);

            mockCommunicator.Verify ();
            Assert.False (result);
        }

        [Fact]
        public void Message_ForwardSingleAgentMessage_ReadMessageFromStreamReturnsNull ()
        {
            var server = new TcpListener (IPAddress.Any, 5000);
            server.Start ();
            var client = new TcpClient ("localhost", 5000);
            var stream = server.AcceptTcpClient ();
            server.Stop ();
            client.Close ();

            var result = Message.ForwardSingleAgentMessage (stream.GetStream (), 2);

            Assert.False (result);
        }

        [Fact]
        public void Message_ForwardSingleAgentMessage_IncorrectMessage ()
        {
            var server = new TcpListener (IPAddress.Any, 5000);
            server.Start ();
            using var client = new TcpClient ("localhost", 5000);
            var stream = server.AcceptTcpClient ().GetStream ();
            server.Stop ();
            var bytes = Encoding.UTF8.GetBytes ("not_json");
            client.GetStream ().Write (BitConverter.GetBytes ((Int16)bytes.Length));
            client.GetStream ().Write (bytes);

            var result = Message.ForwardSingleAgentMessage (stream, 2);

            Assert.True (result);
        }

        [Fact]
        public void Message_ForwardSingleAgentMessage_Success ()
        {
            var server = new TcpListener (IPAddress.Any, 5000);
            server.Start ();
            using var client = new TcpClient ("localhost", 5000);
            var stream = server.AcceptTcpClient ().GetStream ();
            using var gm = new TcpClient ("localhost", 5000);
            var gmClient = server.AcceptTcpClient ();
            typeof (Communicator).GetProperty ("GmClient", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public).SetValue (null, gmClient);

            server.Stop ();
            var bytes = Encoding.UTF8.GetBytes (@"{""messageID"":9,""payload"":{ }}");
            client.GetStream ().Write (BitConverter.GetBytes ((Int16)bytes.Length));
            client.GetStream ().Write (bytes);

            var result = Message.ForwardSingleAgentMessage (stream, 2);

            Assert.True (result);
        }

        [Fact]
        public void Message_ForwardSingleGMMessage_ReadMessageFromStreamReturnsNull ()
        {
            var server = new TcpListener (IPAddress.Any, 5000);
            server.Start ();
            var client = new TcpClient ("localhost", 5000);
            var stream = server.AcceptTcpClient ();
            server.Stop ();
            client.Close ();

            var result = Message.ForwardSingleGMMessage (stream.GetStream (), null);

            Assert.False (result);
        }

        [Fact]
        public void Message_ForwardSingleGMMessage_IncorrectMessage ()
        {
            var server = new TcpListener (IPAddress.Any, 5000);
            server.Start ();
            using var client = new TcpClient ("localhost", 5000);
            var stream = server.AcceptTcpClient ().GetStream ();
            server.Stop ();
            var bytes = Encoding.UTF8.GetBytes ("not_json");
            client.GetStream ().Write (BitConverter.GetBytes ((Int16)bytes.Length));
            client.GetStream ().Write (bytes);

            var result = Message.ForwardSingleGMMessage (stream, new Communicator ());

            Assert.True (result);
        }

        [Fact]
        public void Message_ForwardSingleGMMessage_NoAgentID ()
        {
            var server = new TcpListener (IPAddress.Any, 5000);
            server.Start ();
            using var client = new TcpClient ("localhost", 5000);
            var stream = server.AcceptTcpClient ().GetStream ();

            server.Stop ();
            var bytes = Encoding.UTF8.GetBytes (@"{""messageID"":9,""payload"":{ }}");
            client.GetStream ().Write (BitConverter.GetBytes ((Int16)bytes.Length));
            client.GetStream ().Write (bytes);

            var result = Message.ForwardSingleGMMessage (stream, new Communicator ());

            Assert.True (result);
        }

        [Fact]
        public void Message_ForwardSingleGMMessage_AgentIDNotFound ()
        {
            var server = new TcpListener (IPAddress.Any, 5000);
            server.Start ();
            using var client = new TcpClient ("localhost", 5000);
            var stream = server.AcceptTcpClient ().GetStream ();

            server.Stop ();
            var bytes = Encoding.UTF8.GetBytes (@"{""messageID"":9,""agentID"": 1,""payload"":{ }}");
            client.GetStream ().Write (BitConverter.GetBytes ((Int16)bytes.Length));
            client.GetStream ().Write (bytes);

            var result = Message.ForwardSingleGMMessage (stream, new Communicator ());

            Assert.True (result);
        }

        [Fact]
        public void Message_ForwardSingleGMMessage_ForwardMessage ()
        {
            var server = new TcpListener (IPAddress.Any, 5000);
            server.Start ();
            using var client = new TcpClient ("localhost", 5000);
            var stream = server.AcceptTcpClient ();
            using var gm = new TcpClient ("localhost", 5000);
            var gmClient = server.AcceptTcpClient ();
            server.Stop ();

            var comm = new Communicator ();
            comm.AddAgentClient (stream);
            var temp = comm.Correlation;
            var bytes = Encoding.UTF8.GetBytes ($"{{\"messageID\":9,\"agentID\": {temp.Keys.First ()}}}");
            gm.GetStream ().Write (BitConverter.GetBytes ((Int16)bytes.Length));
            gm.GetStream ().Write (bytes);

            var result = Message.ForwardSingleGMMessage (gmClient.GetStream (), comm);

            Assert.True (result);
        }

        [Fact]
        public void Message_ForwardSingleGMMessage_EndGameMessage ()
        {
            var server = new TcpListener (IPAddress.Any, 5000);
            server.Start ();
            using var client = new TcpClient ("localhost", 5000);
            var stream = server.AcceptTcpClient ();
            using var gm = new TcpClient ("localhost", 5000);
            var gmClient = server.AcceptTcpClient ();
            server.Stop ();

            var mockComm = new Mock<Communicator> ();
            mockComm.Setup (c => c.DropAgent (It.IsAny<int> (), It.IsAny<bool> ())).Verifiable ();
            var comm = mockComm.Object;
            comm.AddAgentClient (stream);
            var temp = comm.Correlation;
            var bytes = Encoding.UTF8.GetBytes ($"{{\"messageID\":104,\"agentID\": {temp.Keys.First ()}}}");
            gm.GetStream ().Write (BitConverter.GetBytes ((Int16)bytes.Length));
            gm.GetStream ().Write (bytes);

            var result = Message.ForwardSingleGMMessage (gmClient.GetStream (), comm);

            Assert.True (result);
            mockComm.Verify ();
        }

        [Fact]
        public void Message_ToString ()
        {
            var message = new Message () { agentID = 2, correlationID = 3, messageID = 4, payload = new { } };

            var s = message.ToString ();

            Assert.Equal ("{\"messageID\":4,\"agentID\":2,\"correlationID\":3,\"payload\":{}}", s.Replace ("\n", "").Replace ("\r", "").Replace (" ",""));
        }
    }
}
