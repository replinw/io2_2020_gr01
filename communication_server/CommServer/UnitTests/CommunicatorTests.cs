﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using Xunit;

namespace UnitTests
{
    public class CommunicatorTests
    {
        [Fact]
        public void Communicator_AddAgentClient ()
        {
            var comm = new CommServer.Communicator (2);

            comm.AddAgentClient (new TcpClient ());

            var temp = comm.Correlation;
            Assert.Single (temp);
        }

        [Fact]
        public void Communicator_DropAgent ()
        {
            var comm = new CommServer.Communicator (3);
            comm.AddAgentClient (new TcpClient ());
            comm.AddAgentClient (new TcpClient ());
            var temp = comm.Correlation;

            comm.DropAgent (temp.First ().Key);

            Assert.Single (temp);
        }

        [Fact]
        public void Communicator_DropAgent_WithCorrelationZero ()
        {
            var comm = new CommServer.Communicator (2);
            comm.AddAgentClient (new TcpClient ());
            comm.SetGMClient (new TcpClient ());
            var temp = comm.Correlation;

            comm.DropAgent (temp.First ().Key);

            Assert.Empty (temp);
        }

        [Fact]
        public void Communicator_Start ()
        {
            var comm = new CommServer.Communicator (3);
            comm.SetGMClient (new TcpClient ());
            comm.AddAgentClient (new TcpClient ());
            comm.AddAgentClient (new TcpClient ());

            comm.Start ();

            var tasks = (List<Task>)comm.GetType ().InvokeMember ("tasks", System.Reflection.BindingFlags.GetField | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic, null, comm, null);
            Assert.Equal (3, tasks.Count);
        }
    }
}
