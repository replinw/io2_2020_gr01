﻿using CommServer;
using Xunit;

namespace UnitTests
{
    public class ConfiguratorTests
    {
        [Theory]
        [InlineData (new string [] {"-v", "-c", "10", "-pa", "5000", "-pg", "5001"}, 5000, 5001, 10, true, false)]
        [InlineData (new string [] { "-v", "-c", "10", "-pg", "5001",  "-pa", "5000", "-f"}, 5000, 5001, 10, true, true)]
        [InlineData (new string [] { "-v", "-pg", "5001", "-pa", "5000", "-c", "--FailOnDrop"}, 5000, 5001, 5, true, true)]
        [InlineData (new string [] { "-v", "-pg", "5001", "-pa", "5000", "-c"}, 5000, 5001, 5, true, false)]
        [InlineData (new string [] { "-v", "-c", "10", "-pg", "5001", "-pa"}, 5000, 5001, 10, true, false)]
        [InlineData (new string [] { "-v", "-c", "10", "-pa", "5000","-pg"}, 5000, 5000, 10, true, false)]
        [InlineData (new string [] {"-c", "10", "-pa", "5000", "-pg", "5001", "-v" }, 5000, 5001, 10, true, false)]
        [InlineData (new string [] {}, 5000, 5000, 5, false, false)]
        [InlineData (new string [] { "10", "-pa", "5000", "-pg", "5001", "-v" }, 5000, 5001, 5, true, false)]
        [InlineData (new string [] { "-c", "10", "-pa", "-pg", "4000", "5001", "-v" }, 5000, 4000, 10, true, false)]
        [InlineData (new string [] { "-c", "10", "-pa", "5000", "dfoahsfouiah", "-pg", "5001", "-v" }, 5000, 5001, 10, true, false)]
        [InlineData (new string [] { "-c", "-55", "-pa", "45", "-pg", "45", "-v" }, 5000, 5000, 5, true, false)]
        public void Configurator_Configure (string [] args, int portAgent, int portGm, int connLimit, bool verbose, bool f)
        {
            var result = Configurator.Configure (args, out int pares, out int pgres, out int clres, out bool vres, out _, out bool failOnDrop);

            Assert.True (result);
            Assert.Equal (portAgent, pares);
            Assert.Equal (portGm, pgres);
            Assert.Equal (clres, connLimit);
            Assert.Equal (verbose, vres);
            Assert.Equal (f, failOnDrop);
        }

        [Theory]
        [InlineData ("-h")]
        [InlineData ("--help")]
        public void Configurator_Configure_Help (string help)
        {
            var result = Configurator.Configure (new string [] { help }, out _, out _, out _, out _, out _, out _);

            Assert.False (result);
        }
    }
}
