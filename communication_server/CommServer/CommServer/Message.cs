﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;

namespace CommServer
{
    public class Message
    {
#pragma warning disable IDE1006 // Naming Styles
        public int messageID { get; set; }
        public int? agentID { get; set; }
        public int? correlationID { get; set; }
        public object payload { get; set; }
#pragma warning restore IDE1006 // Naming Styles
        private static readonly int endGameMessageId = 104;
        public override string ToString () => JsonSerializer.Serialize<Message> (this, new JsonSerializerOptions () { IgnoreNullValues = true, WriteIndented = true });
        public static Message BytesToMessage (byte [] buffer, StreamWriter log = null)
        {
            if (buffer is null)
            {
                return null;
            }
            var utf8Reader = new Utf8JsonReader (buffer);
            Message message;
            try
            {
                message = JsonSerializer.Deserialize<Message> (ref utf8Reader);
            }
            catch (JsonException e)
            {
                if (Program.VerboseMode)
                {
                    Console.WriteLine ($"{DateTime.Now}: Error in JSON message from game master: " + e.Message);
                }
                log?.WriteLine ($"{DateTime.Now}: Error in JSON message from Game Master: " + Encoding.UTF8.GetString (buffer));
                message = null;
            }
            return message;
        }

        public static byte [] ReadMessageFromStream (NetworkStream stream, out byte [] msgSizebuf, out Int16 msgSize)
        {
            msgSizebuf = new byte [Communicator.msgSizeBytes];
            msgSize = 0;
            try
            {
                var i = stream.Read (msgSizebuf, 0, Communicator.msgSizeBytes);
                if (i == 0)
                {
                    return null;
                }
                msgSize = BitConverter.ToInt16 (msgSizebuf);
                var buffer = new byte [msgSize];
                i = stream.Read (buffer, 0, msgSize);
                return i == 0 ? null : buffer;
            }
            catch (Exception ex) when (ex is IOException || ex is ObjectDisposedException)
            {
                return null; //blocking read was interrupted - server exiting
            }
        }

        public static bool WriteMessage (Message message, TcpClient client, int? agentID = null)
        {
            if (message.payload is null)
            {
                message.payload = new { };
            }
            var buffer = JsonSerializer.SerializeToUtf8Bytes (message, new JsonSerializerOptions () { WriteIndented = true, IgnoreNullValues = true });
            try
            {
                var stream = client.GetStream ();
                var sendBuffer = new byte [buffer.Length + 2];
                var length = BitConverter.GetBytes ((Int16)buffer.Length);
                sendBuffer [0] = length [0]; sendBuffer [1] = length [1];
                buffer.CopyTo (sendBuffer, 2);
                if (agentID is null)
                {
                    lock (Communicator.GmClient)
                    {
                        stream.Write (sendBuffer, 0, sendBuffer.Length);
                    }
                }
                else
                {
                    stream.Write (sendBuffer, 0, sendBuffer.Length);
                }
            }
            catch (Exception ex) when (ex is ObjectDisposedException || ex is IOException)
            {
                return false;
            }
            return true;
        }

        public static bool ForwardSingleAgentMessage (NetworkStream stream, int agentID)
        {
            var buffer = Message.ReadMessageFromStream (stream, out _, out _);
            if (buffer is null)
            {
                return false;
            }
            var message = Message.BytesToMessage (buffer);
            if (message is null)
            {
                if (Program.VerboseMode)
                {
                    Console.WriteLine ($"Message from {agentID} was corrupt and couldn't be forwarded.");
                }
                return true;
            }
            if (message.agentID is null || message.agentID != agentID)
            {
                message.agentID = agentID;
            }
            if (Program.VerboseMode)
            {
                Console.WriteLine ($"Message received from agent {message.agentID}. Forwarding to GM. Message: {message}");
            }
            _ = Message.WriteMessage (message, Communicator.GmClient);
            return true;
        }

        public static bool ForwardSingleGMMessage (NetworkStream stream, Communicator communicator)
        {
            var buffer = Message.ReadMessageFromStream (stream, out _, out _);
            if (buffer is null)
            {
                return false;
            }
            var message = Message.BytesToMessage (buffer, communicator.log);
            if (message is null)
            {
                if (Program.VerboseMode)
                {
                    Console.WriteLine ($"Message from GM was corrupt and couldn't be forwarded.");
                }
                return true;
            }
            if (Program.VerboseMode)
            {
                Console.WriteLine ($"Message received from GM. Forwarding to {message.agentID}. Message: {message}");
            }
            if (message.agentID is null || !message.agentID.HasValue)
            {
                return true;
            }
            TcpClient client;
            try
            {
                client = communicator.Correlation [message.agentID.Value];
            }
            catch (KeyNotFoundException)
            {
                return true;
            }
            if (!Message.WriteMessage (message, client, message.agentID.Value))
            {
                if (Program.VerboseMode)
                {
                    Console.WriteLine ($"Failed to send message to {message.agentID}");
                }
                return false;
            }
            if (message.messageID == endGameMessageId)
            {
                communicator.EndGame = true;
                if (Program.VerboseMode)
                {
                    Console.WriteLine ($"EndGame - closing {message.agentID}");
                }
                communicator.DropAgent (message.agentID.Value);
            }
            return !Program.RetryRejected || message.messageID != 107 || ((JsonElement)message.payload).GetProperty ("accepted").GetBoolean ();
        }
    }
}
