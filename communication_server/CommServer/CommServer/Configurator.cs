﻿using System;
using System.IO;
using System.Linq;
using System.Text.Json;

namespace CommServer
{
    public class Configurator
    {
#pragma warning disable IDE1006 // Naming Styles
        public int portAgentow { get; set; }
        public int portGM { get; set; }
#pragma warning restore IDE1006 // Naming Styles
#pragma warning disable S107 // Methods should not have too many parameters
        public static bool Configure (string [] args, out int portAgent, out int portGm, out int connLimit, out bool verboseMode, out bool retryRejected, out bool failOnDrop, string filename = "config.json")
#pragma warning restore S107 // Methods should not have too many parameters
        {
            //default
            portAgent = 5000; portGm = 5000; connLimit = 5; verboseMode = false; retryRejected = true; failOnDrop = false;

            //config file
            if (File.Exists (filename))
            {
                using var fs = File.OpenRead (filename);
                var config = JsonSerializer.DeserializeAsync<Configurator> (fs).Result;
                portAgent = config.portAgentow;
                portGm = config.portGM;
            }

            //parameters
            if (args.Contains ("-h"))
            {
                SendHelp (false);
                return false;
            }
            if (args.Contains ("--help"))
            {
                SendHelp (true);
                return false;
            }
            retryRejected = !args.Contains ("--NoRetryRejected");
            verboseMode = args.Contains ("-v");
            if (args.Contains ("-pa"))
            {
                for (int i = 0; i < args.Length; i++)
                {
                    if (args [i] == "-pa")
                    {
                        if (i == args.Length - 1)
                        {
                            break;
                        }
                        var result = int.TryParse (args [i + 1], out portAgent);
                        if (!result || portAgent < 1024 || portAgent > 65535)
                        {
                            portAgent = 5000;
                        }
                    }
                }
            }
            if (args.Contains ("-pg"))
            {
                for (int i = 0; i < args.Length; i++)
                {
                    if (args [i] == "-pg")
                    {
                        if (i == args.Length - 1)
                        {
                            break;
                        }
                        var result = int.TryParse (args [i + 1], out portGm);
                        if (!result || portGm < 1024 || portGm > 65535)
                        {
                            portGm = 5000;
                        }
                    }
                }
            }
            if (args.Contains ("-c"))
            {
                for (int i = 0; i < args.Length; i++)
                {
                    if (args [i] == "-c")
                    {
                        if (i == args.Length - 1)
                        {
                            break;
                        }
                        var result = int.TryParse (args [i + 1], out connLimit);
                        if (!result || connLimit < 2)
                        {
                            connLimit = 5;
                        }
                    }
                }
            }
            if (args.Contains ("-f") || args.Contains ("--FailOnDrop"))
            {
                failOnDrop = true;
            }
            return true;
        }
        
        private static void SendHelp (bool more)
        {
            if (!more)
            {
                Console.WriteLine ("usage: -pa port -pg port -v -c connections");
            }
            else
            {
                Console.WriteLine ("usage:\n-pa port - integer between 1024 and 65535 - port on which the server is listening for agent connections");
                Console.WriteLine ("usage:\n-pg port - integer between 1024 and 65535 - port on which the server is listening for game master connection");
                Console.WriteLine ("-v verbose mode");
                Console.WriteLine ("-c connection limit - integer greater than 1 - number of all connections: agents + game master");
                Console.WriteLine ("--NoRetryRejected - don't check whether GM accepted an agent");
                Console.WriteLine ("-f --FailOnDrop - CS will crash when any agent disconnects");
            }
        }
    }
}
