﻿using System;
using System.Net;
using System.Net.Sockets;

namespace CommServer
{
    public static class Program
    {
        public static Communicator Communicator { get; private set; }
        private static TcpListener server;
        public static bool VerboseMode { get; private set; }
        public static bool FailOnDrop { get; private set; }
        public static bool RetryRejected { get; private set; }
        static void Main (string [] args)
        {
            if (!Configurator.Configure (args, out int portAgent, out int portGm, out int connLimit, out bool verboseMode, out bool retryRejected, out bool failOnDrop))
            {
                return;
            }
            RetryRejected = retryRejected;
            FailOnDrop = failOnDrop;
            VerboseMode = verboseMode;
            if (VerboseMode)
            {
                Console.WriteLine ($"Agent port set to {portAgent}");
                Console.WriteLine ($"GM port set to {portGm}");
            }
            Console.CancelKeyPress += Console_CancelKeyPress;

            Communicator = new Communicator (connLimit);

            server = new TcpListener (IPAddress.Any, portGm);

            Communicator.ComMain (ref server, portGm, portAgent, connLimit);
        }

        private static void Console_CancelKeyPress (object sender, ConsoleCancelEventArgs e)
        {
            e.Cancel = true;
            server.Stop ();
            Communicator.Cancel ();
            Environment.Exit (0);
        }
    }
}
