﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace CommServer
{
    public class Communicator
    {
        public readonly static int msgSizeBytes = 2;
        internal static TcpClient GmClient { get; private set; }
        public Dictionary<int, TcpClient> Correlation { get; private set; }
        private readonly Random rand = new Random ();
        private readonly List<Task> tasks = new List<Task> ();
        internal readonly StreamWriter log;
        public bool EndGame { get; set; }

        public Communicator () : this (1024) { }
        public Communicator (int connectionLimit, string logName = "errorMessageLog.txt")
        {
#if RELEASE
            log = new System.IO.StreamWriter (logName, true);
#endif
            Correlation = new Dictionary<int, TcpClient> (connectionLimit - 1);
        }

        public static void ComMain (ref TcpListener server, int portGm, int portAgent, int connLimit)
        {
            server.Start ();
            try
            {
                Program.Communicator.SetGMClient (server.AcceptTcpClient ());
                if (Program.VerboseMode)
                {
                    Console.WriteLine ("GM connected");
                }
                if (portGm != portAgent)
                {
                    server.Stop ();
                    server = new TcpListener (IPAddress.Any, portAgent);
                    server.Start ();
                }
                for (int i = 0; i < connLimit - 1; i++)
                {
                    if (!Program.Communicator.AddAgentClient (server.AcceptTcpClient ()))
                    {
                        i--;
                        if (Program.VerboseMode)
                        {
                            Console.WriteLine ("Agent connection refused by GM.");
                        }
                    }
                }
            }
            catch (SocketException)
            {
                if (Program.VerboseMode)
                {
                    Console.WriteLine ("Waiting for clients interrupted.");
                }
                return;
            }
            server.Stop ();
            if (Program.VerboseMode)
            {
                Console.WriteLine ("Starting to forward messages");
            }
            Program.Communicator.Start ();
            Program.Communicator.WaitForEnd ();
        }

        public void SetGMClient (TcpClient client) => GmClient = client;
        public bool AddAgentClient (TcpClient client)
        {
            int newId = rand.Next ();
            while (Correlation.ContainsKey (newId))
            {
                newId = rand.Next ();
            }
            Correlation.Add (newId, client);
            if (Program.RetryRejected && (!Message.ForwardSingleAgentMessage (client.GetStream (), newId) || !Message.ForwardSingleGMMessage (GmClient.GetStream (), this)))
            {
                DropAgent (newId, true);
                return false;
            }
            return true;
        }
        public void Start ()
        {
            var gm = Task.Run (() => AwaitGMMessage ());
            tasks.Add (gm);
            foreach (var item in Correlation)
            {
                var t = Task.Run (() => AwaitAgentMessage (item.Value, item.Key));
                tasks.Add (t);
            }
        }
        public virtual void Cancel ()
        {
            if (EndGame && Correlation.Count != 0)
            {
                return;
            }
            foreach (var item in Correlation)
            {
                item.Value.Close ();
            }
            GmClient?.Close ();
        }

        public void WaitForEnd ()
        {
            Task.WaitAll (tasks.ToArray ());
            if (Program.VerboseMode)
            {
                Console.WriteLine ("All connections closed.");
            }
            log?.Flush ();
            log?.Dispose ();
        }

        private void AwaitGMMessage ()
        {
            NetworkStream stream = null;
            try
            {
                stream = GmClient.GetStream ();
            }
            catch (Exception ex) when (ex is ObjectDisposedException || ex is InvalidOperationException)
            {
                Cancel ();
                return;
            }
            while (Message.ForwardSingleGMMessage (stream, this)) { /*empty loop*/ }
            Cancel ();
        }


        public virtual void DropAgent (int agentID, bool dontClose = false)
        {
            try
            {
                var agent = Correlation [agentID];
                Correlation.Remove (agentID);
                agent.Close ();
            }
            catch (KeyNotFoundException)
            {
                // This means agent has already been removed and no action should be taken
            }
            if (!dontClose && Correlation.Count == 0)
            {
                Cancel ();
            }
        }

        private void AwaitAgentMessage (TcpClient agent, int agentID)
        {
            NetworkStream stream = null;
            try
            {
                stream = agent.GetStream ();
            }
            catch (Exception ex) when (ex is ObjectDisposedException || ex is InvalidOperationException)
            {
                if (Program.FailOnDrop)
                {
                    Cancel ();
                }
                else
                {
                    DropAgent (agentID);
                }
                return;
            }
            while (Message.ForwardSingleAgentMessage (stream, agentID)) { /*empty loop*/ }
            if (Program.FailOnDrop)
            {
                Cancel ();
            }
            else
            {
                DropAgent (agentID);
            }
        }
    }
}
